<?php

use yii\db\Schema;
use yii\db\Migration;

class m150811_051934_captcha extends Migration
{
    public function up()
    {
        $this->createTable('captcha', [
            'id' => $this->primaryKey(),
            'code' => $this->string('32')->notNull(),
            'key' => $this->string('10')->notNull()->defaultValue(''),
            'date' => $this->integer(),
        ]);

    }

    public function down()
    {
        $this->dropTable('captcha');
    }

}
