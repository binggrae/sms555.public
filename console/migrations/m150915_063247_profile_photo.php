<?php

use yii\db\Schema;
use yii\db\Migration;

class m150915_063247_profile_photo extends Migration
{
    public function up()
    {
        $this->addColumn('profile', 'photo', $this->string());

    }

    public function down()
    {
        $this->dropColumn('profile', 'photo');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
