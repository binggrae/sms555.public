<?php

use yii\db\Schema;
use yii\db\Migration;

class m151206_102608_outhe_account extends Migration
{
    public function up()
    {
        $opt = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('outer_account', [

            'id' => $this->primaryKey(),
            'login'=> $this->string()->notNull(),
            'pass' => $this->string()->notNull(),
        ], $opt);
//
        $this->addColumn('order', 'oa_id', $this->integer());

 }

    public function down()
    {
        $this->dropColumn('order', 'oa_id');

        $this->dropTable('outer_account');
    }

}
