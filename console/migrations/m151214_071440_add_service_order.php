<?php

use yii\db\Schema;
use yii\db\Migration;

class m151214_071440_add_service_order extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'service', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('order', 'comment', $this->text()->notNull()->defaultValue(''));

    }

    public function down()
    {
        $this->dropColumn('order', 'service');
        $this->dropColumn('order', 'comment');
    }

}
