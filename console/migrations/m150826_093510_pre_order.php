<?php

use yii\db\Schema;
use yii\db\Migration;

class m150826_093510_pre_order extends Migration
{
    public function up()
    {
        $this->createTable('pre_order', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'account_id' => $this->integer()->defaultValue(NULL),
        ]);
    }

    public function down()
    {
        $this->dropTable('pre_order');
    }

}
