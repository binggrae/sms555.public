<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_140250_order_type extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'type', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('order', 'type');
    }

}
