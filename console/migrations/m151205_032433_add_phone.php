<?php

use yii\db\Schema;
use yii\db\Migration;

class m151205_032433_add_phone extends Migration
{
    public function up()
    {
        $this->addColumn('account', 'phone', $this->string()->defaultValue(null));

    }

    public function down()
    {
        $this->dropColumn('account', 'login');
    }

}
