<?php

use yii\db\Schema;
use yii\db\Migration;

class m151118_111315_sending extends Migration
{
    public function up()
    {
        $opt = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable('sending', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'status' => $this->integer(),
            'message' => $this->text()->notNull(),
        ], $opt);

        $this->createTable('sms', [
            'id' => $this->primaryKey(),
            'sending_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'sms_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()
        ], $opt);

        $this->addForeignKey('idx_sms_sendingId', 'sms', 'sending_id', 'sending', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('idx_sms_sendingId', 'sms');

        $this->dropTable('sending');
        $this->dropTable('sms');
    }

}
