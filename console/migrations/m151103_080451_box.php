<?php

use yii\db\Schema;
use yii\db\Migration;

class m151103_080451_box extends Migration
{
    public function up()
    {
        $this->addColumn('box', 'is_send', $this->integer()->notNull()->defaultValue(0));

    }

    public function down()
    {
        $this->dropColumn('box', 'is_send');
    }

}
