<?php

use yii\db\Schema;
use yii\db\Migration;

class m150925_085753_order_count extends Migration
{
    public function up()
    {
        $this->createTable('box', [
            'id' => $this->primaryKey(),
            'uid' => $this->integer(),
            'price' => $this->string(),
            'name' => $this->string(),
            'link' => $this->string(),
        ]);

        $this->addColumn('account', 'box_count', $this->integer());


    }

    public function down()
    {
        echo "m150925_085753_order_count cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
