<?php

use yii\db\Schema;
use yii\db\Migration;

class m151021_061028_accounts extends Migration
{
    public function up()
    {
        $this->addColumn('account', 'box_id', $this->integer());
        $this->addColumn('account', 'order_count', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('account', 'box_id');
        $this->dropColumn('account', 'order_count');
    }

}
