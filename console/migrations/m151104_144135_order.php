<?php

use yii\db\Schema;
use yii\db\Migration;

class m151104_144135_order extends Migration
{
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'box_id' => $this->integer()->notNull(),
            'account_id' => $this->integer()->defaultValue(null),
            'email' => $this->string()->notNull(),
            'ORDER_PROP_3' => $this->string()->notNull(),
            'ORDER_PROP_7' => $this->string()->notNull(),
            'ORDER_PROP_6_val' => $this->string()->notNull(),
            'ORDER_PROP_6' => $this->string()->notNull(),
            'ORDER_PROP_5' => $this->string()->notNull(),
            'ORDER_PROP_10' => $this->string()->notNull(),
            'ORDER_PROP_11' => $this->string()->notNull(),
            'ORDER_PROP_12' => $this->string()->notNull(),
            'ORDER_PROP_13' => $this->string()->notNull(),
            'TRACKING_NUMBER' => $this->string()->notNull()->defaultValue(''),
            'DELIVERY_ID' => $this->string()->notNull(),
        ]);

        $this->addForeignKey('idx_order_accountId', 'order', 'account_id', 'account', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey('idx_order_accountId', 'order');
        $this->dropTable('order');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
