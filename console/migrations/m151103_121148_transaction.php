<?php

use yii\db\Schema;
use yii\db\Migration;

class m151103_121148_transaction extends Migration
{
    public function up()
    {
        $this->createTable('transaction', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'service' => $this->integer(),
            'period' => $this->integer(),
            'count' => $this->integer(),
        ]);

        $this->addForeignKey('idx_transaction_userId', 'transaction', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');


    }

    public function down()
    {
        $this->dropForeignKey('idx_transaction_userId', 'transaction');
        $this->dropTable('transaction');
    }

}
