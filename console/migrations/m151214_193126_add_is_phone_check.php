<?php

use yii\db\Schema;
use yii\db\Migration;

class m151214_193126_add_is_phone_check extends Migration
{
    public function up()
    {
        $this->addColumn('account', 'is_phone', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
       $this->dropColumn('account', 'is_phone');
    }

}
