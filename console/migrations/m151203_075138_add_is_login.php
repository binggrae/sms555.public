<?php

use yii\db\Schema;
use yii\db\Migration;

class m151203_075138_add_is_login extends Migration
{
    public function up()
    {
        $this->addColumn('account', 'is_login', $this->integer()->notNull()->defaultValue(0));

    }

    public function down()
    {
        $this->dropColumn('account', 'is_login');

    }

}
