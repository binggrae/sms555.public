<?php

use yii\db\Schema;
use yii\db\Migration;

class m150811_041930_account_box extends Migration
{
    public function up()
    {
        $this->createTable('account_box', [
            'id' => $this->primaryKey(),
            'account_id' => $this->integer()->notNull(),
            'box_id' => $this->integer()->notNull(),
            'date' => $this->integer()->notNull(),
            'link' => $this->text(),
            'is_delete' => $this->boolean()->notNull()->defaultValue(false)
        ]);

        $this->addForeignKey('AccountBox_accountId', 'account_box', 'account_id', 'account', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey('AccountBox_accountId', 'account_box');

        $this->dropTable('account_box');
    }

}
