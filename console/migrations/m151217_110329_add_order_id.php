<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_110329_add_order_id extends Migration
{
    public function up()
    {
        $this->addColumn('account_box', 'order_id', $this->integer()->defaultValue(null));
        $this->addColumn('account_box', 'price', $this->string()->notNull()->defaultValue(''));

    }

    public function down()
    {
        $this->dropColumn('account_box', 'order_id');
        $this->dropColumn('account_box', 'price');
    }

}
