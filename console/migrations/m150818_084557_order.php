<?php

use yii\db\Schema;
use yii\db\Migration;

class m150818_084557_order extends Migration
{
    public function up()
    {
        $this->addColumn('account_box', 'is_order', $this->boolean()->defaultValue(0)->notNull());

    }

    public function down()
    {
        $this->dropColumn('account_box', 'is_order');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
