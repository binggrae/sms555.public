<?php

use yii\db\Schema;
use yii\db\Migration;

class m151207_111131_load_phone extends Migration
{
    public function up()
    {
        $accounts = array(
            array( // row #0
                'id' => 1063,
                'phone' => '79035475181',
            ),
            array( // row #1
                'id' => 1064,
                'phone' => '79099846351',
            ),
            array( // row #2
                'id' => 1065,
                'phone' => '79099845722',
            ),
            array( // row #3
                'id' => 1066,
                'phone' => '79636271501',
            ),
            array( // row #4
                'id' => 1067,
                'phone' => '79096975181',
            ),
            array( // row #5
                'id' => 1068,
                'phone' => '79099020447',
            ),
            array( // row #6
                'id' => 1069,
                'phone' => '79636239253',
            ),
            array( // row #7
                'id' => 1070,
                'phone' => '79032507829',
            ),
            array( // row #8
                'id' => 1071,
                'phone' => '79096892118',
            ),
            array( // row #9
                'id' => 1072,
                'phone' => '79636277584',
            ),
            array( // row #10
                'id' => 1073,
                'phone' => '79035474818',
            ),
            array( // row #11
                'id' => 1074,
                'phone' => '79035475135',
            ),
            array( // row #12
                'id' => 1075,
                'phone' => '79035480695',
            ),
            array( // row #13
                'id' => 1076,
                'phone' => '79035523348',
            ),
            array( // row #14
                'id' => 1077,
                'phone' => '79037080136',
            ),
            array( // row #15
                'id' => 1078,
                'phone' => '79035471958',
            ),
            array( // row #16
                'id' => 1079,
                'phone' => '79035705143',
            ),
            array( // row #17
                'id' => 1080,
                'phone' => '79032463418',
            ),
            array( // row #18
                'id' => 1081,
                'phone' => '79035615756',
            ),
            array( // row #19
                'id' => 1082,
                'phone' => '79096875156',
            ),
            array( // row #20
                'id' => 1280,
                'phone' => '79032525247',
            ),
            array( // row #21
                'id' => 1281,
                'phone' => '79032055431',
            ),
            array( // row #22
                'id' => 1282,
                'phone' => '79629985585',
            ),
            array( // row #23
                'id' => 1283,
                'phone' => '79685378545',
            ),
            array( // row #24
                'id' => 1284,
                'phone' => '79651521318',
            ),
            array( // row #25
                'id' => 1285,
                'phone' => '79091581505',
            ),
            array( // row #26
                'id' => 1286,
                'phone' => '79031614483',
            ),
            array( // row #27
                'id' => 1287,
                'phone' => '79686178706',
            ),
            array( // row #28
                'id' => 1289,
                'phone' => '79099851883',
            ),
            array( // row #29
                'id' => 1290,
                'phone' => '79067579539',
            ),
            array( // row #30
                'id' => 1291,
                'phone' => '79067608599',
            ),
            array( // row #31
                'id' => 1292,
                'phone' => '79035688379',
            ),
            array( // row #32
                'id' => 1293,
                'phone' => '79035683261',
            ),
            array( // row #33
                'id' => 1294,
                'phone' => '79663528755',
            ),
            array( // row #34
                'id' => 1295,
                'phone' => '79661870745',
            ),
            array( // row #35
                'id' => 1296,
                'phone' => '79647737727',
            ),
            array( // row #36
                'id' => 1297,
                'phone' => '79654101462',
            ),
            array( // row #37
                'id' => 1299,
                'phone' => '79099846496\t',
            ),
            array( // row #38
                'id' => 1535,
                'phone' => '79099404732',
            ),
            array( // row #39
                'id' => 1536,
                'phone' => '79629277729',
            ),
            array( // row #40
                'id' => 1537,
                'phone' => '79032015069',
            ),
            array( // row #41
                'id' => 1538,
                'phone' => '79685378525',
            ),
            array( // row #42
                'id' => 1539,
                'phone' => '79652584593',
            ),
            array( // row #43
                'id' => 1540,
                'phone' => '79647726302',
            ),
            array( // row #44
                'id' => 1541,
                'phone' => '79654102717',
            ),
            array( // row #45
                'id' => 1542,
                'phone' => '79686178050',
            ),
            array( // row #46
                'id' => 1543,
                'phone' => '79686178700',
            ),
            array( // row #47
                'id' => 1544,
                'phone' => '79651607274',
            ),
            array( // row #48
                'id' => 1545,
                'phone' => '79629602068',
            ),
            array( // row #49
                'id' => 1546,
                'phone' => '79629602176',
            ),
            array( // row #50
                'id' => 1547,
                'phone' => '79099463469',
            ),
            array( // row #51
                'id' => 1548,
                'phone' => '79035698496',
            ),
            array( // row #52
                'id' => 1549,
                'phone' => '79629612785\t',
            ),
            array( // row #53
                'id' => 1550,
                'phone' => '79686178868',
            ),
            array( // row #54
                'id' => 1551,
                'phone' => '79661864543',
            ),
            array( // row #55
                'id' => 1552,
                'phone' => '79647738677',
            ),
            array( // row #56
                'id' => 1553,
                'phone' => '79091581318',
            ),
            array( // row #57
                'id' => 1792,
                'phone' => '79032056443',
            ),
            array( // row #58
                'id' => 1793,
                'phone' => '79032027412',
            ),
            array( // row #59
                'id' => 1794,
                'phone' => '79685378475',
            ),
            array( // row #60
                'id' => 1795,
                'phone' => '79651521182',
            ),
            array( // row #61
                'id' => 1796,
                'phone' => '79685378190',
            ),
            array( // row #62
                'id' => 1797,
                'phone' => '79639920164',
            ),
            array( // row #63
                'id' => 1798,
                'phone' => '79686178262',
            ),
            array( // row #64
                'id' => 1799,
                'phone' => '79652584462',
            ),
            array( // row #65
                'id' => 1800,
                'phone' => '79652584067',
            ),
            array( // row #66
                'id' => 1801,
                'phone' => '79099463775',
            ),
            array( // row #67
                'id' => 1802,
                'phone' => '79099463421',
            ),
            array( // row #68
                'id' => 1803,
                'phone' => '79035687742',
            ),
            array( // row #69
                'id' => 1804,
                'phone' => '79035693728',
            ),
            array( // row #70
                'id' => 1805,
                'phone' => '79686179475',
            ),
            array( // row #71
                'id' => 1806,
                'phone' => '79663553919',
            ),
            array( // row #72
                'id' => 1807,
                'phone' => '79661864543',
            ),
            array( // row #73
                'id' => 1808,
                'phone' => '79099841727',
            ),
        );

        foreach ($accounts as $account) {
            $this->update('account', ['phone' => $account['phone']], ['id' => $account['id']]);
        }

    }

    public function down()
    {
        echo "m151207_111131_load_phone cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
