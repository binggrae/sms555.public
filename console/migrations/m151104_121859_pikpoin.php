<?php

use yii\db\Schema;
use yii\db\Migration;

class m151104_121859_pikpoin extends Migration
{
    public function up()
    {
        $this->createTable('pikpoint', [
            'id' => $this->primaryKey(),
            'address' => $this->string()->notNull(),
            'city' => $this->string()->notNull(),
            'house' => $this->string()->notNull(),
            'uid' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'region' => $this->string()->notNull(),
            'street' => $this->string()->notNull(),
            'code' => $this->integer()->notNull(),
        ]);

    }

    public function down()
    {
        $this->dropTable('pikpoint');
    }


}
