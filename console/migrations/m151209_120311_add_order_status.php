<?php

use yii\db\Schema;
use yii\db\Migration;

class m151209_120311_add_order_status extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'status', $this->integer()->notNull()->defaultValue(0));

    }

    public function down()
    {
        $this->dropColumn('order', 'status');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
