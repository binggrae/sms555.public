<?php

use yii\db\Schema;
use yii\db\Migration;

class m151206_110321_add_is_login extends Migration
{
    public function up()
    {
        $this->addColumn('outer_account', 'is_login', $this->integer()->notNull()->defaultValue(0));

    }

    public function down()
    {
        $this->dropColumn('outer_account', 'is_login');
    }

}
