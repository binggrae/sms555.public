<?php

use yii\db\Schema;
use yii\db\Migration;

class m151214_194918_add_is_deelte extends Migration
{
    public function up()
    {
        $this->addColumn('account', 'is_delete', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('account', 'is_delete');
    }
}
