<?php

use yii\db\Schema;
use yii\db\Migration;

class m151103_080826_drop_user_data extends Migration
{
    public function up()
    {
        $this->dropForeignKey('UserData_UserId_User_Id', 'user_data');
        $this->dropTable('user_data');

        $this->addColumn('user', 'active_to', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->createTable('user_data', [
            'user_id' => $this->integer()->notNull(),
            'is_sms' => $this->integer(2)->notNull()->defaultValue(0),
            'is_vk' => $this->integer(2)->notNull()->defaultValue(0),
            'sms_at' => $this->integer()->notNull(),
            'vk_at' => $this->integer()->notNull(),
        ]);
        $this->createIndex('UserData_UserId', 'user_data', 'user_id', 1);
        $this->addForeignKey('UserData_UserId_User_Id', 'user_data', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

    }

}
