<?php

use yii\db\Schema;
use yii\db\Migration;

class m151201_130932_import_transaction extends Migration
{
    public function up()
    {
        $transaction = array(
            array('id' => '10', 'user_id' => '5550715', 'created_at' => '1430759769', 'service' => '1', 'count' => '2', 'package_id' => '1', 'period' => '30'),
            array('id' => '12', 'user_id' => '13280617', 'created_at' => '1431364638', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '13', 'user_id' => '275864155', 'created_at' => '1431366608', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '14', 'user_id' => '16285972', 'created_at' => '1431375784', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '15', 'user_id' => '304816041', 'created_at' => '1431430122', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '16', 'user_id' => '11434200', 'created_at' => '1431430814', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '17', 'user_id' => '711588', 'created_at' => '1431437170', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '18', 'user_id' => '2387822', 'created_at' => '1431440413', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '23', 'user_id' => '8637894', 'created_at' => '1431500061', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '24', 'user_id' => '244605050', 'created_at' => '1431501847', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '25', 'user_id' => '304913086', 'created_at' => '1431502819', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '27', 'user_id' => '229481002', 'created_at' => '1431518424', 'service' => '2', 'count' => '550', 'package_id' => '1', 'period' => '90'),
            array('id' => '28', 'user_id' => '12142715', 'created_at' => '1431587368', 'service' => '2', 'count' => '550', 'package_id' => '1', 'period' => '90'),
            array('id' => '29', 'user_id' => '18879243', 'created_at' => '1431704547', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '30', 'user_id' => '43882340', 'created_at' => '1431927268', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '31', 'user_id' => '4246567', 'created_at' => '1432031718', 'service' => '2', 'count' => '550', 'package_id' => '1', 'period' => '90'),
            array('id' => '33', 'user_id' => '1327633', 'created_at' => '1432064917', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '34', 'user_id' => '17928151', 'created_at' => '1432067737', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '35', 'user_id' => '231113168', 'created_at' => '1432147245', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '36', 'user_id' => '305913322', 'created_at' => '1432215365', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '37', 'user_id' => '1598044', 'created_at' => '1432233706', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '38', 'user_id' => '38209992', 'created_at' => '1432334170', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '39', 'user_id' => '306216236', 'created_at' => '1432376990', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '40', 'user_id' => '1366464', 'created_at' => '1432554078', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '41', 'user_id' => '9430117', 'created_at' => '1432584077', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '42', 'user_id' => '227713875', 'created_at' => '1432633058', 'service' => '1', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '43', 'user_id' => '84447856', 'created_at' => '1432684324', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '44', 'user_id' => '26521805', 'created_at' => '1432801059', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '45', 'user_id' => '244374504', 'created_at' => '1432919919', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '46', 'user_id' => '8042632', 'created_at' => '1433326192', 'service' => '1', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '47', 'user_id' => '5550715', 'created_at' => '1433914200', 'service' => '3', 'count' => '0', 'package_id' => '2', 'period' => '90'),
            array('id' => '48', 'user_id' => '301047966', 'created_at' => '1433968355', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '49', 'user_id' => '301047966', 'created_at' => '1434214060', 'service' => '3', 'count' => '0', 'package_id' => '1', 'period' => '7'),
            array('id' => '50', 'user_id' => '52909722', 'created_at' => '1434358353', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '51', 'user_id' => '231113168', 'created_at' => '1434549214', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '52', 'user_id' => '18879243', 'created_at' => '1434741744', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '53', 'user_id' => '306216236', 'created_at' => '1434799944', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '54', 'user_id' => '38209992', 'created_at' => '1434913836', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '55', 'user_id' => '275864155', 'created_at' => '1434917721', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '56', 'user_id' => '17928151', 'created_at' => '1434950766', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '57', 'user_id' => '1598044', 'created_at' => '1434957523', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '58', 'user_id' => '2387822', 'created_at' => '1434971763', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '59', 'user_id' => '244605050', 'created_at' => '1434978969', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '60', 'user_id' => '223014676', 'created_at' => '1434985338', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '61', 'user_id' => '84447856', 'created_at' => '1435184697', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '62', 'user_id' => '711588', 'created_at' => '1435228844', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '63', 'user_id' => '159017266', 'created_at' => '1435296259', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '64', 'user_id' => '247662172', 'created_at' => '1436100041', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '65', 'user_id' => '227713875', 'created_at' => '1436140573', 'service' => '1', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '66', 'user_id' => '223014676', 'created_at' => '1436860324', 'service' => '1', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '67', 'user_id' => '301047966', 'created_at' => '1437250337', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '68', 'user_id' => '231113168', 'created_at' => '1437305914', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '69', 'user_id' => '304816041', 'created_at' => '1437335213', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '70', 'user_id' => '18879243', 'created_at' => '1437571316', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '71', 'user_id' => '7541013', 'created_at' => '1438307755', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '72', 'user_id' => '70664246', 'created_at' => '1438445437', 'service' => '1', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '73', 'user_id' => '711588', 'created_at' => '1438462516', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '74', 'user_id' => '8148508', 'created_at' => '1438517222', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '75', 'user_id' => '138552000', 'created_at' => '1438524901', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '76', 'user_id' => '3221808', 'created_at' => '1438586054', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '77', 'user_id' => '14804568', 'created_at' => '1438678835', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '78', 'user_id' => '5768980', 'created_at' => '1438959289', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '79', 'user_id' => '5550715', 'created_at' => '1439002772', 'service' => '3', 'count' => '0', 'package_id' => '1', 'period' => '90'),
            array('id' => '80', 'user_id' => '227713875', 'created_at' => '1439410217', 'service' => '1', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '81', 'user_id' => '105686570', 'created_at' => '1439548138', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '82', 'user_id' => '34641621', 'created_at' => '1439550232', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '83', 'user_id' => '2762040', 'created_at' => '1439578084', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '84', 'user_id' => '247662172', 'created_at' => '1439640950', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '85', 'user_id' => '4246567', 'created_at' => '1439800480', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '86', 'user_id' => '4246567', 'created_at' => '1439801100', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '87', 'user_id' => '4246567', 'created_at' => '1439804721', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '88', 'user_id' => '223014676', 'created_at' => '1440316411', 'service' => '1', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '89', 'user_id' => '223014676', 'created_at' => '1440317031', 'service' => '1', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '90', 'user_id' => '223014676', 'created_at' => '1440320652', 'service' => '1', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '91', 'user_id' => '231113168', 'created_at' => '1440421845', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '92', 'user_id' => '18879243', 'created_at' => '1440436426', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '93', 'user_id' => '1327633', 'created_at' => '1440488063', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '94', 'user_id' => '305913322', 'created_at' => '1440489326', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '95', 'user_id' => '11696595', 'created_at' => '1440516075', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '96', 'user_id' => '1996781', 'created_at' => '1440582178', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '97', 'user_id' => '1109470', 'created_at' => '1440595636', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '98', 'user_id' => '11696595', 'created_at' => '1440600872', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '99', 'user_id' => '7541013', 'created_at' => '1441018257', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '100', 'user_id' => '51596001', 'created_at' => '1441190580', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '101', 'user_id' => '229481002', 'created_at' => '1441450777', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '102', 'user_id' => '17928151', 'created_at' => '1441880631', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '103', 'user_id' => '227713875', 'created_at' => '1441982517', 'service' => '1', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '104', 'user_id' => '304816041', 'created_at' => '1442221566', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '105', 'user_id' => '84447856', 'created_at' => '1442251172', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '106', 'user_id' => '2762040', 'created_at' => '1442494748', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '107', 'user_id' => '14804568', 'created_at' => '1442567685', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '108', 'user_id' => '1598044', 'created_at' => '1442735375', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '109', 'user_id' => '231113168', 'created_at' => '1442923354', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '110', 'user_id' => '51560175', 'created_at' => '1444928792', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '111', 'user_id' => '227713875', 'created_at' => '1444932818', 'service' => '1', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '112', 'user_id' => '18879243', 'created_at' => '1445099941', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '113', 'user_id' => '14804568', 'created_at' => '1445227784', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '114', 'user_id' => '8042632', 'created_at' => '1445428012', 'service' => '1', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '115', 'user_id' => '26514288', 'created_at' => '1445442147', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '116', 'user_id' => '84447856', 'created_at' => '1446068586', 'service' => '2', 'count' => '550', 'package_id' => '3', 'period' => '90'),
            array('id' => '117', 'user_id' => '275864155', 'created_at' => '1446470061', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30'),
            array('id' => '118', 'user_id' => '331483425', 'created_at' => '1446487661', 'service' => '2', 'count' => '250', 'package_id' => '1', 'period' => '30'),
            array('id' => '119', 'user_id' => '65944269', 'created_at' => '1446578707', 'service' => '3', 'count' => '0', 'package_id' => '3', 'period' => '90'),
            array('id' => '120', 'user_id' => '5550715', 'created_at' => '1446745780', 'service' => '3', 'count' => '0', 'package_id' => '2', 'period' => '90'),
            array('id' => '121', 'user_id' => '275864155', 'created_at' => '1446874961', 'service' => '3', 'count' => '0', 'package_id' => '2', 'period' => '30'),
            array('id' => '122', 'user_id' => '227713875', 'created_at' => '1446875011', 'service' => '3', 'count' => '0', 'package_id' => '2', 'period' => '30'),
            array('id' => '123', 'user_id' => '1996781', 'created_at' => '1446875144', 'service' => '3', 'count' => '0', 'package_id' => '1', 'period' => '30'),
            array('id' => '124', 'user_id' => '1598044', 'created_at' => '1446875186', 'service' => '3', 'count' => '0', 'package_id' => '1', 'period' => '30'),
            array('id' => '125', 'user_id' => '18879243', 'created_at' => '1447873769', 'service' => '2', 'count' => '100', 'package_id' => '2', 'period' => '30')
        );
        
        foreach ($transaction as $item) {

            $this->insert('transaction', [
                'id' => $item['id'],
                'user_id' => $item['user_id'],
                'created_at' => $item['created_at'],
                'service' => $item['service'],
                'count' => $item['count'],
                'period' => $item['period']
            ]);

        }


    }

    public function down()
    {
        echo "m151201_130932_import_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
