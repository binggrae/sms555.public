<?php

use yii\db\Schema;
use yii\db\Migration;

class m151125_113011_delete_order extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'is_delete', $this->integer()->notNull()->defaultValue(0));

    }

    public function down()
    {
        $this->dropColumn('order', 'is_delete');
    }

}
