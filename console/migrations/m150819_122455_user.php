<?php

use yii\db\Schema;
use yii\db\Migration;

class m150819_122455_user extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'auth_key' => $this->string(32)->notNull(),
            'phone' => $this->string()->notNull()->defaultValue(''),
            'role' => $this->integer(3)->notNull()->defaultValue(0),
            'status' => $this->integer(3)->notNull()->defaultValue(0),
            'is_active' => $this->integer(1)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'last_login' => $this->integer()->notNull(),
            'test_count' => $this->integer()->notNull()->defaultValue(2),
        ]);

        $this->createTable('profile', [
            'user_id' => $this->integer()->notNull(),
            'username' => $this->string()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'gender' => $this->integer(2)->notNull(),
            'city' => $this->integer()->notNull(),
            'country' => $this->integer()->notNull(),
            'city_str' => $this->string()->notNull(),
            'country_str' => $this->string()->notNull(),
        ]);
        $this->createIndex('Profile_UserId', 'profile', 'user_id', 1);
        $this->addForeignKey('Profile_UserId_User_Id', 'profile', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('user_data', [
            'user_id' => $this->integer()->notNull(),
            'is_sms' => $this->integer(2)->notNull()->defaultValue(0),
            'is_vk' => $this->integer(2)->notNull()->defaultValue(0),
            'sms_at' => $this->integer()->notNull(),
            'vk_at' => $this->integer()->notNull(),
        ]);
        $this->createIndex('UserData_UserId', 'user_data', 'user_id', 1);
        $this->addForeignKey('UserData_UserId_User_Id', 'user_data', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey('UserData_UserId_User_Id', 'user_data');
        $this->dropForeignKey('Profile_UserId_User_Id', 'profile');

        $this->dropTable('user');
        $this->dropTable('profile');
        $this->dropTable('user_data');
    }

}
