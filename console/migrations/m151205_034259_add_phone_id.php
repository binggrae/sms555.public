<?php

use yii\db\Schema;
use yii\db\Migration;

class m151205_034259_add_phone_id extends Migration
{
    public function up()
    {
        $this->addColumn('account', 'phone_id', $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('account', 'phone_id');

    }

}
