<?php

use yii\db\Schema;
use yii\db\Migration;

class m150811_041650_accounts extends Migration
{
    public function up()
    {
        $this->createTable('account', [
            'id' => $this->primaryKey(),
            'login' => $this->string('32')->notNull(),
            'password' => $this->string('32')->notNull()->defaultValue(''),
            'mail_password' => $this->string('32')->notNull(),
            'cookie'=>$this->text(),
        ]);

    }

    public function down()
    {
        $this->dropTable('account');
    }

}
