<?php

use yii\db\Schema;
use yii\db\Migration;

class m151112_203555_person extends Migration
{
    public function up()
    {
        $this->createTable('person', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'sort' => $this->integer()->notNull()->defaultValue(0)
        ]);

    }

    public function down()
    {
        $this->dropTable('person');
    }
}
