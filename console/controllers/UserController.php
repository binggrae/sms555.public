<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 22.11.2015
 * Time: 17:46
 */

namespace console\controllers;


use common\models\Sending;
use common\models\User;
use yii\console\Controller;

class UserController extends Controller
{
    public function actionDeactivate()
    {
        /** @var User[] $users */
        $users = User::getDeactivatedUser()->all();

        if ($users) {
            foreach ($users as $user) {
                $user->deactivate();
            }
        }
    }

    public function actionNotify()
    {
        /** @var User[] $users */
        $users = User::getNotifiedUser()->all();

        if ($users) {
            foreach ($users as $user) {
                $message = $user->profile->first_name . ', Ваша подписка заканчивается ' .
                    \Yii::t('app', '{n, plural, =0{сегодня} =1{через # день} other{через # дня}}', [
                        'n' => $user->getActiveDay()
                    ]);
                Sending::send([$user->phone], $message);
            }
        }
    }
}