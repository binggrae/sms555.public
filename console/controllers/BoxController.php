<?php

namespace console\controllers;

use common\components\Watcher;
use common\models\Account;
use common\models\AccountBox;
use common\models\Box;
use yii\console\Controller;


class BoxController extends Controller
{

    /**
     * Ловля последней коробки
     * @param null|integer $id
     */
    public function actionNew($id = null)
    {
        $index = 10;
        $conf = [];
        while (1) {
            $watcher = new Watcher(['id' => $id]);

            if ($index == 10) {
                $conf = $watcher->base();
                $index = 0;
                Watcher::dump($conf);
            } else {
                $watcher->order($conf);
            }
        }
    }

    /**
     * Ловля коробки по указанному id
     * @param $id
     */
    public function actionStart($id)
    {
        $conf = [];
        while (1) {
            $watcher = new Watcher(['id' => $id]);

            $conf = $watcher->base($conf);
            $count = $watcher->check($conf);
            if ($count > 0) {
                $watcher->order($conf);
            } elseif ($count) {
                $watcher->deleteAccount();
            }
        }

    }

//    START CAPTCHA
//    public function actionNew($id = null)
//    {
//        while (1) {
//            $watcher = new Watcher($id);
//            $conf = $watcher->base([]);
//            $count = $watcher->check($conf);
//            if ($count > 0) {
//                Watcher::dump("order {$conf['id']}");
//                $watcher->order($conf);
//            } else {
//                sleep(5);
//            }
//        }
//    }
//    END CAPTCHA


    /**
     * Оформление заказа
     */
    public function actionOrder()
    {
        while (1) {
            /** @var AccountBox $box */
            $box = AccountBox::find()->where('is_order=0')->orderBy('id DESC')->one();
            if (!$box) {
                sleep(5);
                continue;
            }

            $box->order();
        }
    }

    /**
     * Загрузка
     */
    public function actionLoadOrder()
    {
        while (1) {
            /** @var AccountBox[] $boxes */
            $boxes = AccountBox::getOrderBox(true)
                ->andWhere('order_id is null')
                ->orderBy('id DESC')
                ->all();

            foreach ($boxes as $box) {
                if (!$box->loadData()) {
                    $box->order_id = 0;
                    $box->save();
                };
            }
            sleep(60);
        }
    }


    /**
     * @param null $id
     * @return bool
     */
    public function actionDrop($id = null)
    {
        $query = AccountBox::find()
            ->leftJoin('order', 'account_box.account_id = order.account_id')
            ->where(['is_order' => 1])
            ->andWhere('order.id is null');

        if ($id) {
            $query->andWhere('box_id=:box_id', [':box_id' => $id]);
        }

        /** @var AccountBox $box */
        $box = $query->one();

        if ($box) {
            if ($box->drop()) {
                Watcher::dump("Delete {$box->id} order");
            } else {
                Watcher::dump("NOT Delete {$box->id} order");
            }

            return true;
        } else {
            return false;
        }
    }


    /**
     * @param null $id
     */
    public function actionMultiDrop($id = null)
    {
        while (1) {
            if (!$this->actionDrop($id)) {
                break;
            }
            sleep(2);
        }

        Watcher::dump("End multi-drop");
    }


    /**
     * @param bool $is_end
     */
    public function actionCheck($is_end = true)
    {

        /** @var AccountBox[] $boxes */
        $boxes = AccountBox::getOrderBox()->all();

        foreach ($boxes as $box) {
            $box->check();
        }

        if(!$is_end) {
            Watcher::dump("End check. Sleep 3600");
            sleep(3600);
            $this->actionCheck($is_end);
        }
    }

    /**
     * Наблюдене за выходом коробки
     */
    public function actionObserve()
    {
        while (1) {
            $watcher = new Watcher();
            $data = $watcher->base();

            $box = Box::find()->where(['uid' => $data['id']])->one();

            if (!$box) {
                Box::createBox($data['id'], $data['title'], $data['link']);
            }
            Watcher::dump('No new box. sleep 30');
            sleep(30);
        }

    }

}