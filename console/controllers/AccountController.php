<?php

namespace console\controllers;

use common\components\Watcher;
use common\models\Account;
use common\models\Order;
use console\models\Person;
use yii\console\Controller;
use yii\helpers\VarDumper;

class AccountController extends Controller
{

    public function actionImport()
    {
        $text = file_get_contents(\Yii::getAlias('@console/data/') . 'import.txt');
        $strings = explode("\r", $text);

        foreach ($strings as $string) {
            $math = explode(';', $string);
            $model = new Account([
                'login' => trim($math[0]),
            ]);
            $model->save();
        }
    }

    public function actionPerson()
    {
        $text = file_get_contents(\Yii::getAlias('@console/data/') . 'import.txt');
        $strings = explode("\r", $text);

        foreach ($strings as $string) {
            $math = explode(';', $string);
            $model = new Person([
                'name' => trim($math[3]) . ' ' . trim($math[2]),
            ]);
            $model->save();
        }

    }


    public function actionRegister()
    {
        /** @var Account[] $accounts */
        $accounts = Account::find()->orderBy('id DESC')->where('password = ""')->all();

        foreach ($accounts as $account) {
            Watcher::dump("Register account : {$account->login}");
            $account->runRegister();
        }
    }

    public function actionLogin($is_end = true)
    {
        /** @var Account[] $accounts */
        $accounts = Account::getUnLoginAccount()->all();

        foreach ($accounts as $account) {
            Watcher::dump("Login account : {$account->login}");
            $account->runLogin();
        }

        if(!$is_end) {
            Watcher::dump("End login. Sleep 60");
            sleep(60);
            $this->actionLogin($is_end);
        }

    }

    public function actionCheck()
    {
        /** @var Account[] $accounts */
        $accounts = Account::find()->all();
        $banned = 0;

        foreach ($accounts as $account) {
            $result = Watcher::get('http://elenakrygina.com/user/box/', $account->id);

            $reg = '/class="bx-system-auth-form"/';
            preg_match($reg, $result, $match);

            if (!empty($match)) {
                $banned++;
                Watcher::dump("{$account->id}#{$account->login} is banned!!!!");
                $account->delete();
            } else {
                Watcher::dump("{$account->id}#{$account->login} is god");
            }

        }

        Watcher::dump("Banned: {$banned} accounts");
    }


    /**
     * todo not work
     */
    public function actionPhone()
    {
        /** @var Account[] $accounts */
        $accounts = Account::find()->where('box_id is null AND is_login=1 AND phone is null')->all();

        foreach ($accounts as $account) {

            $account->createPhone();

            $code = $account->sendPhone();

            if ($code) {
                $account->sendCode($code);
            }

            die();
        }
    }

    /**
     * todo not work
     * @param $id
     * @param $code
     */
    public function actionCode($id, $code)
    {
        /** @var Account $account */
        $account = Account::findOne(['id' => $id]);

        VarDumper::dump($account->login);
        VarDumper::dump($code);

        $account->sendCode($code);
    }

    public function actionCheckPhone($is_end = true)
    {
        /** @var Account[] $accounts */
        $accounts = Account::getUnConfirmedAccount()->all();

        foreach($accounts as $account) {
            $result = Watcher::get('http://elenakrygina.com/user/', $account->id);
            $regexp = '/value\=\"\+(\d{11})\"/';
            preg_match($regexp, $result, $match);

            if (count($match) == 2) {
                Watcher::dump("Account: {$account->login} is activate");
                $account->is_phone = 1;
                $account->save(0);
            } else {
                Watcher::dump("Account: {$account->login} is not activate");
            }
        }

        if(!$is_end) {
            Watcher::dump("End check phone. Sleep 60");
            sleep(60);
            $this->actionCheckPhone($is_end);
        }
    }
}