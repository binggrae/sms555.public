<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 23.11.2015
 * Time: 13:38
 */

namespace console\controllers;


use common\components\Watcher;
use common\models\Sending;
use common\models\Sms;
use yii\console\Controller;

class SmsController extends Controller
{

    public function actionCheck($is_end = true)
    {

        /** @var Sms[] $messages */
        $messages = Sms::find()
            ->where('status = :status', [':status' => Sms::STATUS_WAIT])
            ->all();

        if ($messages) {
            $ids = [];
            foreach ($messages as $message) {
                $ids[] = $message->sms_id;
            }

            $result = Watcher::post('http://api.infosmska.ru/interfaces/GetMessagesState.ashx', 0, [
                'login' => \Yii::$app->params['sms_login'],
                'pwd' => md5(\Yii::$app->params['sms_password']),
                'ids' => implode(',', $ids),
            ]);

            preg_match_all('/([\-0-9]+)/', $result, $matches);
            $matches = $matches[1];

            foreach ($messages as $i => $message) {
                $message->status = Sms::reformStatusCode($matches[$i]);
                $message->save(0);
            }

            /* @var Sending[] $sending */
            $sending = Sending::find()
                ->where('status != :status', [':status' => Sending::STATUS_FINISH])
                ->with('waitSms')
                ->all();

            foreach ($sending as $item) {
                $item->status = $item->waitSms ? Sending::STATUS_PROCESS : Sending::STATUS_FINISH;
                $item->save(0);
            }
        }

        if(!$is_end) {
            sleep(60);
            $this->actionCheck($is_end);
        }
    }

}