<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 04.11.2015
 * Time: 17:22
 */

namespace console\controllers;

use common\models\Pikpoint;
use yii\console\Controller;
use yii\helpers\VarDumper;

class PikpointController extends Controller
{

    public function actionLoad()
    {
        $ch = curl_init('http://elenakrygina.com/bitrix/templates/.default/components/idesigning/sale.order.ajax/ircit_sale_order/postamats.php');
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => 1,
        ]);

        $result = json_decode(curl_exec($ch));

        echo 'count: ' . count($result->GLOBAL) . "\n";


        foreach ($result->GLOBAL as $item) {

            switch ($item->ID) {
                case '7702-001' :
                    $house = 'д. 10';
                    continue;
                case '6512-001' :
                    $house = 'д. 20';
                    continue;
                case '6662-003' :
                    $house = 'д. 52';
                    continue;
                case '2552-001' :
                    $house = 'д. 2 В';
                    continue;
                case '2522-003' :
                    $house = 'д. 40';
                    continue;
                case '4702-012' :
                    $house = 'д. 5';
                    continue;
                case '6192-002' :
                    $house = 'д. 12/1';
                    continue;
                case '6192-003' :
                    $house = 'д. 34';
                    continue;
                case '6192-004' :
                    $house = 'д. 1';
                    continue;
                case '6192-005' :
                    $house = 'д. 2';
                    continue;
                case '6192-009' :
                    $house = 'д. 56';
                    continue;
                case '2392-005' :
                    $house = 'д. 20';
                    continue;
                case '3492-001' :
                    $house = 'д. 54 А';
                    continue;
                case '3492-002' :
                    $house = 'д. 60';
                    continue;
                case '2532-003' :
                    $house = 'д. 32А';
                    continue;
                case '4702-015' :
                    $house = 'д. 16/15';
                    continue;
                default:
                    $house = $item->HOUSE;
            }

            $street = $item->STREET == '' ? 'ул. Б.Советская' : $item->STREET;

            $model = new Pikpoint([
                'address' => $item->ADDRESS,
                'city' => $item->CITY,
                'house' => $house,
                'uid' => $item->ID,
                'name' => $item->NAME,
                'region' => $item->REGION,
                'street' => $street,
                'code' => $item->ZIP_CODE,

            ]);


            if (!$model->save()) {
                var_dump($item);
                echo "case '{$model->uid}' : \$house = 'д. '; continue; \n";
            }
        }
        echo 'end';

    }
}