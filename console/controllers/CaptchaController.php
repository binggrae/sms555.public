<?php

namespace console\controllers;

use common\models\Captcha;
use yii\console\Controller;

class CaptchaController extends Controller
{
    public $dir;

    public function beforeAction($action)
    {
        $this->dir = \Yii::getAlias('@console/data/captcha/');

        return parent::beforeAction($action);
    }

    public function actionGenerate()
    {
        while (1) {

            $count = Captcha::find()->where('date > :time', [':time' => time() - 5400])->count();

            if ($count < 10) {
                echo "generate \n";
                $this->generate();
            } else {
                sleep(1);
            }
        }
    }


    public function actionTest()
    {
        $path = $this->dir . '0a5dec779267386ec911d0fd00750add.jpg';
        $path = $this->dir . '0a0b76969dcea2646b2a30b7ff0d3797.jpg';

        $im = @imagecreatefromjpeg($path);

        $height = imagesy($im);
        $width = imagesx($im);

        $binary = [];
        for ($i = 1; $i < $height - 2; $i++) {
            for ($j = 1; $j < $width - 2; $j++) {
                $rgb = imagecolorat($im, $j, $i);
                list($r, $g, $b) = array_values(imageColorsForIndex($im, $rgb));
                $binary[$i][$j] = ($r + $g + $b < 700) ? 1 : 0;
            }
        }

        for ($i = 0; $i < $height; $i++) {
            echo "\n";
            for ($j = 0; $j < $width; $j++) {
                echo $binary[$i][$j];
            }
        }

        $cord_x = [];
        $is_x_start = 0;
        for ($j = 0; $j < $width; $j++) {
            $is_x_cont = 0;
            for ($i = 0; $i < $height; $i++) {

                if ($binary[$i][$j] == 1) {
                    $is_x_cont = 1; // Если есть вхождение, значит идет буква

                    if ($is_x_start == 0) { // Если буква только начата
                        $is_x_start = 1;
                        $cord_x[] = $j;
                    }
                    break;
                }
            }

            if ($is_x_start == 1 && $is_x_cont == 0) { // Если буква начата, но вхождений нет
                $is_x_start = 0;
                $cord_x[] = $j;
            }
        }

        // ПРОВЕРКА ПО ВЫСОТЕ
        $cord_y = [];
        for ($k = 0; $k < count($cord_x); $k += 2) {

            $is_y_start = 0;
            for ($i = 0; $i < $height; $i++) {

                $is_y_cont = 0;
                for ($j = $cord_x[$k]; $j < $cord_x[$k + 1]; $j++) {

                    if ($binary[$i][$j] == 1) {
                        $is_y_cont = 1; // Если есть вхождение, значит идет буква

                        if ($is_y_start == 0) { // Если буква только начата
                            $is_y_start = 1;
                            $cord_y[] = $i;
                        }
                        break;

                    }
                }

                if ($is_y_start == 1 && $is_y_cont == 0) { // Если буква начата, но вхождений нет
                    $is_y_start = 0;
                    $cord_y[] = $i;
                }

            }

        }


        $cord = [];
        for ($k = 0; $k < count($cord_x); $k += 2) {
            $cord[] = [
                'x1' => $cord_x[$k],
                'y1' => $cord_y[$k],
                'x2' => $cord_x[$k + 1],
                'y2' => $cord_y[$k + 1]
            ];
        }

//        foreach ($cord as $item) {
//
//
//            for ($i = $item['y1']; $i < $item['y2']; $i++) {
//                echo "\n";
//                for ($j = $item['x1']; $j < $item['x2']; $j++) {
//                    echo $binary[$i][$j];
//                }
//            }
//            echo "\n";
//            echo "\n";
//        }

    }


    protected function generate()
    {
        $model = new Captcha();
        $model->generate();
    }
}