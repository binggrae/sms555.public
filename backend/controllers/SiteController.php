<?php
namespace backend\controllers;

use backend\models\MapForm;
use backend\models\RemoveForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionMap()
    {
        $model = new MapForm();

        if (Yii::$app->request->isPost) {
            $model->one_file = UploadedFile::getInstance($model, 'one_file');
            $model->two_file = UploadedFile::getInstance($model, 'two_file');
            if ($model->upload()) {
                $result = '';

                $first = file_get_contents($model->one_new);
                $first = explode("\n", $first);

                $two = file_get_contents($model->two_new);
                $two = explode("\n", $two);

                foreach ($first as $f) {
                    foreach ($two as $t) {
                        $str = $f . ' ' . $t;
                        $str = str_replace("\r", "", $str);
                        if (substr_count($str, ' ') < 7) {
                            $result .= $str . "\n";
                        }
                    }
                }

                $model->result_name = uniqid() . '.txt';
                $model->result = 'data/map/result/' . $model->result_name;
                file_put_contents(Yii::getAlias('@backend/web/') . $model->result, $result);
                unlink($model->one_new);
                unlink($model->two_new);
            }
        }

        return $this->render('map', ['model' => $model]);
    }

    public function actionRemove()
    {
        $model = new RemoveForm();

        if (Yii::$app->request->isPost) {
            $model->word_file = UploadedFile::getInstance($model, 'word_file');
            $model->minus_file = UploadedFile::getInstance($model, 'minus_file');
            if ($model->upload()) {
                $result = '';

                $first = file_get_contents($model->word_new);
                $first = explode("\n", $first);


                $two = file_get_contents($model->minus_new);
                $two = explode("\n", $two);

                foreach ($first as $f) {
                    $temp = str_replace("\r", "", $f);
                    $temp = str_replace("\n", "", $temp) . ' ';
                    foreach ($two as $t) {
                        $t2 = str_replace("\r", "", $t);
<<<<<<< HEAD
						$t2 = str_replace("\n", "", $t2);
                        if (strpos($temp, $t2 . ' ') === false) {
	
=======
                        if (strpos($f . ' ', $t2 . ' ') === false) {
>>>>>>> 8831d8839ee7b4eba846ec83c9be35fa8f3ace31
                            $temp .= ' -' . $t2;
                        }
                    }
                    $result .= $temp . "\n";
                }

                $model->result_name = uniqid() . '.txt';
                $model->result = 'data/remove/result/' . $model->result_name;
                file_put_contents(Yii::getAlias('@backend/web/') . $model->result, $result);
                unlink($model->word_new);
                unlink($model->minus_new);
            }
        }

        return $this->render('remove', ['model' => $model]);

    }

    public function actionClear($type, $file)
    {
        unlink(Yii::getAlias("@backend/web/data/{$type}/result/") . $file);
        $this->redirect('/site/' . $type);
    }

}
