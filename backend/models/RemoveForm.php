<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Login form
 */
class RemoveForm extends Model
{

    /**
     * @var UploadedFile
     */
    public $word_file;
    public $word_new;

    /**
     * @var UploadedFile
     */
    public $minus_file;
    public $minus_new;

    public $result;
    public $result_name;

    public function rules()
    {
        return [
            [['word_file', 'minus_file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'txt'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->word_new = Yii::getAlias('@backend/web/data/map/' .uniqid() . '.txt');
            $this->minus_new = Yii::getAlias('@backend/web/data/map/' . uniqid() . '.txt');

            $this->word_file->saveAs($this->word_new);
            $this->minus_file->saveAs($this->minus_new);
            return true;
        } else {
            return false;
        }
    }


}
