<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Login form
 */
class MapForm extends Model
{

    /**
     * @var UploadedFile
     */
    public $one_file;
    public $one_new;

    /**
     * @var UploadedFile
     */
    public $two_file;
    public $two_new;

    public $result;
    public $result_name;

    public function rules()
    {
        return [
            [['one_file', 'two_file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'txt'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->one_new = Yii::getAlias('@backend/web/data/map/' . uniqid() . '.txt');
            $this->two_new = Yii::getAlias('@backend/web/data/map/' . uniqid() . '.txt');

            $this->one_file->saveAs($this->one_new);
            $this->two_file->saveAs($this->two_new);
            return true;
        } else {
            return false;
        }
    }


}
