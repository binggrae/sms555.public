<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\RemoveForm */


$this->title = 'Минуса';
?>
<div class="site-index">
    <div class="body-content">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

        <?= $form->field($model, 'word_file')->fileInput() ?>

        <?= $form->field($model, 'minus_file')->fileInput() ?>


        <button>Submit</button>

        <?php if ($model->result) : ?>
            <div>Result: <a href="/<?=$model->result;?>"><?=$model->result;?></a></div>

            <div>Remove: <a href="<?= Url::to(['/site/clear', 'type' => 'remove', 'file' => $model->result_name]);?>">link</a></div>
        <?php endif; ?>

        <?php ActiveForm::end() ?>

    </div>
</div>


