<?php
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MapForm */


$this->title = 'Слияние';
?>
<div class="site-index">
    <div class="body-content">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

        <?= $form->field($model, 'one_file')->fileInput() ?>

        <?= $form->field($model, 'two_file')->fileInput() ?>


        <button>Submit</button>

        <?php if ($model->result) : ?>
            <div>Result: <a href="/<?=$model->result;?>"><?=$model->result;?></a></div>

            <div>Remove: <a href="<?= Url::to(['/site/clear', 'type' => 'map', 'file' => $model->result_name]);?>">link</a></div>

        <?php endif; ?>

        <?php ActiveForm::end() ?>

    </div>
</div>


