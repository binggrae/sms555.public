<?php

namespace common\models;

use common\components\Watcher;
use Yii;
use yii\base\ErrorException;
use yii\helpers\VarDumper;
use yii\web\HttpException;

/**
 * This is the model class for table "sending".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $status
 * @property string $message
 *
 * @property Sms[] $sms
 * @property Sms[] $waitSms
 */
class Sending extends \yii\db\ActiveRecord
{
    public $quantity;

    const STATUS_NEW = 0;
    const STATUS_PROCESS = 1;
    const STATUS_FINISH = 2;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sending';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'required'],
            [['created_at', 'status'], 'integer'],
            [['message'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата',
            'status' => 'Статус',
            'message' => 'Текст',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSms()
    {
        return $this->hasMany(Sms::className(), ['sending_id' => 'id']);
    }

    public function getWaitSms()
    {
        return $this->hasMany(Sms::className(), ['sending_id' => 'id'])->where(['status' => Sms::STATUS_WAIT]);
    }

    public static function getStatusesArray() {
        return [
            self::STATUS_NEW => 'Новая',
            self::STATUS_PROCESS => 'В процессе',
            self::STATUS_FINISH => 'Завершена',
        ];
    }

    public function beforeSave($insert)
    {
        if (!$this->created_at) {
            $this->created_at = time();
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param array $numbers
     * @param string $message
     * @return bool
     * @throws HttpException
     */
    public static function send($numbers, $message)
    {
        $sending = new Sending([
            'created_at' => time(),
            'status' => self::STATUS_NEW,
            'message' => $message
        ]);

        if ($sending->save()) {
            $numbers_str = [];
            foreach ($numbers as $item) {
                if ($item != '') {
                    $numbers_str[] = preg_replace("/[^0-9]/", '', $item);
                }
            }

            $data = Watcher::post('http://api.infosmska.ru/interfaces/SendMessages.ashx', 0, [
                'login' => \Yii::$app->params['sms_login'],
                'pwd' => md5(\Yii::$app->params['sms_password']),
                'phones' => implode(',', $numbers_str),
                'message' => $message,
                'sender' => 'sms555.ru'
            ]);

            $status = explode(':', $data)[0];

            if ($status == 'Ok') {
                preg_match_all('/(\d+)/', $data, $match);
                $match = $match[1];

                foreach ($match as $i => $sms_id) {
                    $user = User::findOne(['phone' => $numbers[$i]]);
                    if ($user) {
                        $model = new Sms([
                            'sms_id' => $sms_id,
                            'sending_id' => $sending->id,
                            'user_id' => $user->id,
                            'created_at' => time(),
                            'status' => Sms::STATUS_WAIT,
                        ]);
                        if (!$model->save()) {
                            throw new HttpException('Sending.SMS.Save');
                        }
                    }
                }
            } else {
                throw new HttpException('Sending.SMS.BadStatus');
            }
        }

        return true;
    }

    public static function sendByActive($message)
    {
        /** @var User $users */
        $users = User::find()->where('status = :status', [':status' => User::STATUS_ACTIVE])->all();

        self::send(self::getNumbers($users), $message);
    }


    public static function sendByInactive($message)
    {
        /** @var User $users */
        $users = User::find()->where('status = :status', [':status' => User::STATUS_INACTIVE])->all();

        self::send(self::getNumbers($users), $message);
    }

    public static function sendAll($message)
    {
        /** @var User $users */
        $users = User::find()->all();

        self::send(self::getNumbers($users), $message);
    }

    public static function getNumbers($users)
    {
        $numbers = [];
        foreach ($users as $user) {
            if($user->phone) {
                $numbers[] = $user->phone;
            }
        }
        return $numbers;

    }


}
