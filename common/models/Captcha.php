<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "captcha".
 *
 * @property integer $id
 * @property string $code
 * @property string $key
 * @property integer $date
 */
class Captcha extends \yii\db\ActiveRecord
{
    public $image;

    const LINK = 'elenakrygina.com/bitrix/tools/captcha.php?captcha_sid=';

    public $is_phrase = 0;
    public $is_regsense = 0;
    public $is_numeric = 0;
    public $min_len = 5;
    public $max_len = 5;
    public $is_russian = 0;

    public $is_verbose = false;
    public $rtimeout = 5;
    public $mtimeout = 120;

    //839fdbe8954791df716aad77b47683cb
    public $api_key;

    public function init()
    {
        $this->api_key = \Yii::$app->params['antigate_api'];
        return parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'captcha';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['date'], 'integer'],
            [['code'], 'string', 'max' => 32],
            [['key'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'key' => 'Код',
            'image' => 'Картинка',
            'date' => 'Дата',
        ];
    }

    public function beforeSave($insert) {
        if($insert) {
            $this->date = time();
        }

        return parent::beforeSave($insert);
    }

    public function createCode()
    {
        $this->code = md5(time() + rand(1000, 9999));
    }

    public function getFile()
    {
        return \Yii::getAlias('@console/data/captcha/') . $this->code . '.jpg';
    }

    public function getLink()
    {
        return 'http://elenakrygina.com/bitrix/tools/captcha.php?captcha_sid=' . $this->code;
    }

    public function generate()
    {
        $this->createCode();

        file_put_contents($this->getFile(), file_get_contents($this->getLink()));


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://antigate.com/in.php");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'method' => 'post',
            'key' => $this->api_key,
            'file' => '@' . $this->getFile(),
            'phrase' => $this->is_phrase,
            'regsense' => $this->is_regsense,
            'numeric' => $this->is_numeric,
            'min_len' => $this->min_len,
            'max_len' => $this->max_len,
            'is_russian' => $this->is_russian
        ]);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            if ($this->is_verbose) echo "CURL returned error: " . curl_error($ch) . "\n";
        }
        curl_close($ch);
        if (strpos($result, "ERROR") !== false) {
            if ($this->is_verbose) echo "server returned error: $result\n";
        } else {
            $ex = explode("|", $result);
            $captcha_id = $ex[1];
            if ($this->is_verbose) echo "captcha sent, got captcha ID $captcha_id\n";
            $waittime = 0;
            if ($this->is_verbose) echo "waiting for $this->rtimeout seconds\n";
            sleep($this->rtimeout);
            while (true) {
                $result = file_get_contents("http://antigate.com/res.php?key=" . $this->api_key . '&action=get&id=' . $captcha_id);
                if (strpos($result, 'ERROR') !== false) {
                    if ($this->is_verbose) echo "server returned error: $result\n";
                }
                if ($result == "CAPCHA_NOT_READY") {
                    if ($this->is_verbose) echo "captcha is not ready yet\n";
                    $waittime += $this->rtimeout;
                    if ($waittime > $this->mtimeout) {
                        if ($this->is_verbose) echo "timelimit ($this->mtimeout) hit\n";
                        break;
                    }
                    if ($this->is_verbose) echo "waiting for $this->rtimeout seconds\n";
                    sleep($this->rtimeout);
                } else {
                    $ex = explode('|', $result);
                    if (trim($ex[0]) == 'OK') {
                        $this->key = trim($ex[1]);
                        break;
                    }
                }
            }

        }

        $this->save();

    }
}
