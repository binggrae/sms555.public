<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 15.09.2015
 * Time: 11:18
 */

namespace common\models;

use yii\base\NotSupportedException;
use yii\db\ActiveRecord;

/**
 * Class UserAuth
 * @package common\models
 * @property string $auth_key
 */
class UserAuth extends ActiveRecord
{
    public static function tableName()
    {
        return 'user';
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function generateAuthKey()
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }
}