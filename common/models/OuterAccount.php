<?php

namespace common\models;

use common\components\Watcher;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "outer_account".
 *
 * @property integer $id
 * @property string $login
 * @property string $pass
 * @property integer $is_login
 */
class OuterAccount extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'outer_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'pass'], 'required'],
            [['is_login'], 'integer'],
            [['login', 'pass'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'pass' => 'Пароль',
        ];
    }

    public function auth()
    {

        Watcher::post('http://elenakrygina.com/user/?login=yes&newuser=yes', $this->id, [
            'backurl' => '/user/?newuser=yes',
            'AUTH_FORM' => 'Y',
            'TYPE' => 'AUTH',
            'USER_LOGIN' => $this->login,
            'USER_PASSWORD' => $this->pass,
            'USER_REMEMBER' => 'Y',
            'Login' => 'Войти'
        ]);

        $this->is_login = 1;
        $this->save(0);
    }
}
