<?php

namespace common\models;

use console\models\Person;
use v2\modules\admin\models\ManualOrder;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $box_id
 * @property integer $type
 * @property integer $account_id
 * @property integer $status
 * @property integer $service
 * @property string $comment
 * @property string $email
 * @property string $ORDER_PROP_3
 * @property string $ORDER_PROP_7
 * @property string $ORDER_PROP_6_val
 * @property string $ORDER_PROP_6
 * @property string $ORDER_PROP_5
 * @property string $ORDER_PROP_10
 * @property string $ORDER_PROP_11
 * @property string $ORDER_PROP_12
 * @property string $ORDER_PROP_13
 * @property string $TRACKING_NUMBER
 * @property string $DELIVERY_ID
 * @property integer $is_delete
 * @property integer $oa_id
 *
 * @property Box $box
 * @property Account $account
 * @property OuterAccount $oa
 */
class Order extends \yii\db\ActiveRecord
{

    const TYPE_POINT = 1;
    const TYPE_MANUAL = 2;
    const TYPE_DELIVERY = 3;

    const STATUS_NEW = 0;
    const STATUS_ORDER = 1;
    const STATUS_WAIT = 2;
    const STATUS_CLOSE = 3;
    const STATUS_DELETE = 4;

    const SERVICE_NONE = 0;
    const SERVICE_WHATS_APP = 1;
    const SERVICE_VIBER = 2;
    const SERVICE_EMAIL = 3;
    const SERVICE_SMS = 4;
    const SERVICE_IG = 5;

    const DEFAULT_CITY = 'Москва, Московская обла, Россия';
    const DEFAULT_CITY_ID = 3341;
    const DEFAULT_CODE = 101000;

    const MODEL_NAME = 'order';


    public $PH_CONFIRM;
    public $ORDER_PROP_3_CP;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['box_id', 'ORDER_PROP_3', 'ORDER_PROP_7', 'ORDER_PROP_6_val', 'ORDER_PROP_6', 'ORDER_PROP_5', 'ORDER_PROP_10', 'ORDER_PROP_11', 'ORDER_PROP_12', 'ORDER_PROP_13', 'DELIVERY_ID'], 'required'],
            [['box_id', 'account_id', 'type', 'is_delete', 'oa_id', 'status', 'service'], 'integer'],
            [['TRACKING_NUMBER'], 'safe'],
            [['comment', 'email', 'ORDER_PROP_3', 'ORDER_PROP_7', 'ORDER_PROP_6_val', 'ORDER_PROP_6', 'ORDER_PROP_5', 'ORDER_PROP_10', 'ORDER_PROP_11', 'ORDER_PROP_12', 'ORDER_PROP_13', 'DELIVERY_ID'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'status' => 'Статус',
            'service' => 'Сервис',
            'comment' => 'Коммент',
            'box_id' => 'Коробка',
            'account_id' => 'Аккаунт',
            'email' => 'Email',
            'ORDER_PROP_3' => 'ФИО',
            'ORDER_PROP_7' => 'Телефон',
            'ORDER_PROP_6_val' => 'Город',
            'ORDER_PROP_6' => 'ID города',
            'ORDER_PROP_5' => 'Индекс',
            'ORDER_PROP_10' => 'Улица',
            'ORDER_PROP_11' => 'Дом',
            'ORDER_PROP_12' => 'Корпус',
            'ORDER_PROP_13' => 'Квартира',
            'TRACKING_NUMBER' => 'Пикпоинт',
            'DELIVERY_ID' => 'Доставка',
        ];
    }

    /** return Box */
    public function getBox()
    {
        return $this->hasOne(Box::className(), ['id' => 'box_id']);
    }

    /** return Account */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['id' => 'account_id']);
    }

    /** OuterAccount */
    public function getOa()
    {

        return $this->hasOne(Account::className(), ['id' => 'oa_id']);
    }

    public static function getTypesLabel()
    {
        return [
            self::TYPE_POINT => 'Пикпоинт',
            self::TYPE_MANUAL => 'Самовывоз',
            self::TYPE_DELIVERY => 'Курьер',
        ];
    }

    public function getTypeLabel()
    {
        $types = self::getTypesLabel();

        return $types[$this->type];
    }

    public static function getStatusesLabel()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_ORDER => 'Заказан',
            self::STATUS_WAIT => 'Ожидает оплату',
            self::STATUS_CLOSE => 'Закрыт',
            self::STATUS_DELETE => 'Удален',
        ];
    }

    public function getStatusLabel()
    {
        $status = self::getStatusesLabel();

        return $status[$this->status];
    }

    public static function getServicesLabel()
    {
        return [
            self::SERVICE_NONE => 'Нет данных',
            self::SERVICE_WHATS_APP => 'Whats app',
            self::SERVICE_VIBER => 'Viber',
            self::SERVICE_EMAIL => 'Email',
            self::SERVICE_SMS => 'Sms',
            self::SERVICE_IG => 'Instagram',

        ];
    }

    public function getServiceLabel()
    {
        $service = self::getServicesLabel();

        return $service[$this->service];
    }

    public function getTypeLink()
    {
        $types = [
            self::TYPE_POINT => 'point',
            self::TYPE_MANUAL => 'manual',
            self::TYPE_DELIVERY => 'delivery',
        ];

        return $types[$this->type];
    }


    public function beforeSave($insert)
    {
        if ($insert) {
            $this->ORDER_PROP_7 = self::editPhone($this->ORDER_PROP_7);
        }
        return parent::beforeSave($insert);
    }

    public static function editPhone($phone)
    {
        $mask = '#';
        $phone = substr(preg_replace("/[^0-9]/", '', $phone), 1);

        $format = '+7 (###) ### - ####';

        $pattern = '/' . str_repeat('([0-9])?', substr_count($format, $mask)) . '(.*)/';

        $format = preg_replace_callback(
            str_replace('#', $mask, '/([#])/'),
            function () use (&$counter) {
                return '${' . (++$counter) . '}';
            },
            $format
        );

        return trim(preg_replace($pattern, $format, $phone, 1));
    }

    public static function getOrderData($model, $data)
    {
        if (!$model) {
            /** @var Person $person */
            $person = Person::find()->orderBy('sort')->one();
            $person->sort++;
            $person->save();

            $model = new Order([
                'ORDER_PROP_3' => $person->name,
                'ORDER_PROP_7' => self::editPhone('8964' . rand(1000000, 9999999)),
                'ORDER_PROP_6_val' => self::DEFAULT_CITY,
                'ORDER_PROP_6' => self::DEFAULT_CITY_ID,
                'ORDER_PROP_5' => self::DEFAULT_CODE,
                'DELIVERY_ID' => ManualOrder::DELIVERY_ID,
            ]);
        }

        $model->PH_CONFIRM = $model->ORDER_PROP_7;
        $result = $model->getAttributes([
            'ORDER_PROP_3',
            'ORDER_PROP_7',
            'PH_CONFIRM',
            'ORDER_PROP_6_val',
            'ORDER_PROP_6',
            'ORDER_PROP_5',
            'ORDER_PROP_10',
            'ORDER_PROP_11',
            'ORDER_PROP_12',
            'ORDER_PROP_13',
            'DELIVERY_ID',
            'TRACKING_NUMBER',
        ]);

        foreach ($result as &$item) {
            $item = mb_convert_encoding($item, "windows-1251", "UTF-8");
        }
        unset($item);

        return ArrayHelper::merge($result, [
            'sessid' => $data['sessid'],
            'ORDER_PROP_2' => $data['ORDER_PROP_2'],
            'PERSON_TYPE' => 2,
            'PERSON_TYPE_OLD' => 2,
            'showProps' => 'N',
            'BUYER_STORE' => 0,
            'PAY_SYSTEM_ID' => 1,
            'additional' => $data['additional'],
            'confirmorder' => 'N',
            'profile_change' => 'N',
            'is_ajax_post' => 'Y',
            'ACCEPT_USER_SESS' => 'Y',
            'save' => 'Y',
        ]);
    }

    public static function getConfirmedData($data)
    {
        return ArrayHelper::merge($data, [
            'confirmorder' => 'Y',
        ]);
    }

    public static function getOrderProperty($result)
    {
        $result = mb_convert_encoding($result, "UTF-8", "windows-1251");

        /*
        $regexpForm = '/<form action=\"\/box\/personal\/order\/\" method=\"POST\" name=\"ORDER_FORM\" ' .
            'id=\"ORDER_FORM\" enctype=\"multipart\/form-data\">' .
            '(.+)' .
            '<\/form>/';
        preg_match($regexpForm, $result, $match);
        $result = $match[1];
        */

        preg_match('/name=\"sessid\" id=\"sessid\" value=\"([0-9a-z]+)\"/', $result, $match);
        $sessid = $match[1];

        preg_match('/type=\"hidden\" name=\"additional\" value=\"([0-9a-zA-Z=]+)\"/', $result, $match);
        $additional = isset($match[1]) ? $match[1] : '';

        return [
            'sessid' => $sessid,
            'additional' => $additional
        ];
    }

    public static function getConfirmedProperty($result)
    {
        $result = mb_convert_encoding($result, "UTF-8", "windows-1251");

        preg_match('/type=\"hidden\" name=\"additional\" value=\"([0-9a-zA-Z=]+)\"/', $result, $match);
        $additional = $match[1];

        return [
            'additional' => $additional
        ];
    }


    public function search($params)
    {
        $query = Order::find()->where('is_delete=0')->orderBy('id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
		if(!$this->status) {
			$query->andWhere('status != :status', [':status' => self::STATUS_CLOSE]);
		}

        $query->andFilterWhere([
            'id' => $this->id,
            'box_id' => $this->box_id,
            'type' => $this->type,
            'status' => $this->status,
        ]);

        if ($this->account_id == 2) {
            $query->andWhere('account_id is not null');
        } elseif ($this->account_id == 1) {
            $query->andWhere('account_id is null');
        }


        return $dataProvider;
    }

}

