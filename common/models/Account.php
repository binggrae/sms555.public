<?php

namespace common\models;

use common\components\Watcher;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "account".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $mail_password
 * @property string $cookie
 * @property string $phone
 * @property string $phone_id
 * @property integer $box_id
 * @property integer $box_count
 * @property integer $is_login
 * @property integer $is_phone
 * @property integer $is_delete
 * @property integer $order_count
 *
 * @property AccountBox[] $accountBoxes
 */
class Account extends \yii\db\ActiveRecord
{

    const SCENARIO_OTHER_ACCOUNT = 'other_account';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'mail_password'], 'required'],
            [['order_count', 'box_count', 'box_id', 'order_count', 'is_login', 'is_phone', 'is_delete'], 'integer'],
            [['cookie', 'phone', 'phone_id'], 'string'],
            [['login', 'password', 'mail_password'], 'string', 'max' => 32]
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_OTHER_ACCOUNT] = [['login', 'password'], 'string'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Пароль',
            'mail_password' => 'Mail пароль',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountBoxes()
    {
        return $this->hasMany(AccountBox::className(), ['account_id' => 'id']);
    }

    public function runLogin()
    {
        Watcher::post('http://elenakrygina.com/user/?login=yes&newuser=yes', $this->id, [
            'backurl' => '/user/?newuser=yes',
            'AUTH_FORM' => 'Y',
            'TYPE' => 'AUTH',
            'USER_LOGIN' => $this->login,
            'USER_PASSWORD' => $this->password,
            'USER_REMEMBER' => 'Y',
            'Login' => 'Войти'
        ]);

        $this->is_login = 1;
        $this->save(0);

    }

    public function runRegister()
    {

        $post_data = [];
        $post_data['REGISTER[LOGIN]'] = trim($this->login);
        $post_data['REGISTER[EMAIL]'] = trim($this->login);
        $post_data['REGISTER[PASSWORD]'] = '12pass53';
        $post_data['REGISTER[CONFIRM_PASSWORD]'] = '12pass53';
        $post_data['register_submit_button'] = '1';

        Watcher::post('www.elenakrygina.com/user/?newuser=yes', $this->id, $post_data);

        $this->password = '12pass53';
        $this->save();

    }

    public function createPhone($save = 1)
    {
        $number = Watcher::post('http://sms-activate.ru/stubs/handler_api.php', 0, [
            'api_key' => 'ddBBB61146d67e5d9cc76d35c2052132',
            'action' => 'getNumber',
            'service' => 'ot',
            'forward' => '0',
            'operator' => 'any',
        ]);
        Watcher::dump($number);

        $math = explode(':', $number);

        $this->phone_id = $math[1];
        $this->phone = $math[2];

        if($save) {
            $this->save(0);
        }
    }

    /**
     * todo not work
     * @return bool|string
     */
    public function sendPhone()
    {
        $link = 'http://elenakrygina.com/bitrix/components/harlamoff/fb-identification-phone/ajax_get_code.php';
        $result = Watcher::postVerb($link,
            $this->id, ['phone' => mb_convert_encoding('+' . $this->phone, "windows-1251", "UTF-8")]
        );

        Watcher::dump(json_decode($result));

        while(1) {

            $result = Watcher::post('http://sms-activate.ru/stubs/handler_api.php', 0, [
                'api_key' => 'ddBBB61146d67e5d9cc76d35c2052132',
                'action' => 'getStatus',
                'id' => $this->phone_id
            ]);

            $status = explode(':', $result)[0];

            if($status === 'STATUS_OK') {
                return substr($result, -4, 4);
            }

            Watcher::dump("Status: {$status}. Need sleep");
            sleep(2);
        }

        return false;
    }

    /**
     * todo not work
     * @param $code
     */
    public function sendCode($code)
    {
        $link = 'http://elenakrygina.com/bitrix/components/harlamoff/fb-identification-phone/ajax_check_code.php';
        $result = Watcher::postVerb($link, $this->id, [
            'code' => mb_convert_encoding($code, "windows-1251", "UTF-8")
        ]);

        Watcher::dump(json_decode($result));
    }




    // GETTERS QUERY

    /**
     * @return Query
     */
    public static function getUnLoginAccount(){
        $query = Account::find()
            ->where(['is_login' => 0])
            ->andWhere('box_id is null')
            ->andWhere('is_delete = 0');

        return $query;
    }


    /**
     * @param null|integer $box_id
     * @return Query
     */
    public static function getActiveAccount($box_id = null)
    {
        return 1;
    }

    /**
     * @param bool|null|integer $box_id
     * @return Query
     */
    public static function getUnConfirmedAccount($box_id = null)
    {
        $query = Account::find()
            ->where('phone is not null')
            ->andWhere('is_login = 1')
            ->andWhere('is_delete = 0')
            ->andWhere('is_phone = 0');

        if(false !== $box_id) {
            $query->andWhere('box_id = :box_id', [':box_id' => $box_id]);
        }

        return $query;
    }


}
