<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $service
 * @property integer $period
 * @property double $count
 *
 * @property User $user
 */
class Transaction extends ActiveRecord
{

    const SERVICE_YM = 1;
    const SERVICE_CARD = 2;
    const SERVICE_MANUAL = 3;

    const PRICE_30 = 100.00;
    const PRICE_90 = 250.00;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'service', 'count', 'period'], 'required'],
            [['user_id', 'created_at', 'service', 'period'], 'integer'],
            [['count'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'created_at' => 'Дата',
            'service' => 'Способ пополнения',
            'count' => 'Сумма',
            'period' => 'Период'
        ];
    }

    public function getServiceName()
    {
        $services = self::getServiceArray();
        return isset($services[$this->service]) ? $services[$this->service] : '';
    }

    public static function getServiceArray()
    {
        return [
            self::SERVICE_CARD => 'Visa/MasterCard',
            self::SERVICE_YM => 'Яндекс.Деньги',
            self::SERVICE_MANUAL => 'Ввод администратором',
        ];
    }


    public static function getServiceCode() {
        return [
            self::SERVICE_YM => 'PC',
            self::SERVICE_CARD => 'AC',
        ];
    }

    public static function getPaymentArray() {
        return [
            self::getServiceCode()[self::SERVICE_CARD] => 'платеж с карты VISA или MasterCard (Maestro)',
            self::getServiceCode()[self::SERVICE_YM] => 'платеж из кошелька в Яндекс.Деньгах',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public function afterSave($insert, $changedAttributes) {
        if(!$insert) {
            return;
        }

        $this->user->activate($this->period);
    }
}
