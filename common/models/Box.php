<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "box".
 *
 * @property integer $id
 * @property integer $uid
 * @property string $price
 * @property string $name
 * @property string $link
 * @property integer $is_send
 */
class Box extends \yii\db\ActiveRecord
{

    /** @var  Account */
    public $account;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'box';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'is_send'], 'integer'],
            [['price', 'name', 'link'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'price' => 'Price',
            'name' => 'Name',
            'link' => 'Link',
            'is_send' => 'Is Send',
        ];
    }

    /**
     * @return Account|bool
     */
    public function loadAccount()
    {

        $query = Account::find()
            ->join('LEFT JOIN', 'account_box', 'account.id = account_box.account_id')
            ->join('LEFT JOIN', 'order', 'account.id = order.account_id')
            ->where('account_box.id is null')
            ->andWhere('order.id is null')
            ->andWhere('account.phone is not null')
            ->andWhere('account.cookie is null')
            ->andWhere('account.is_login = 1')
            ->orderBy('account.box_count ASC');

        if (is_null($this->uid)) {
            $query->andWhere('account.box_id is null');
        } else {
            $query->andWhere('account.box_id = :id')
                ->params([':id' => $this->uid]);
        }

        $this->account = $query->one();


        return $this->account;
    }


    public static function getList()
    {
        return ArrayHelper::merge(
            [0 => 'Новая KryginaBox'],
            ArrayHelper::map(Box::find()->orderBy('id DESC')->all(), 'id', 'name')
        );
    }

    /**
     * @param $id
     * @param $title
     * @param $link
     */
    public static function createBox($id, $title, $link)
    {
        $box = new Box([
            'uid' => $id,
            'price' => '1',
            'name' => mb_convert_encoding($title, "UTF-8", "windows-1251"),
            'link' => $link,
            'is_send' => 0,
        ]);
        if ($box->save()) {
            Order::updateAll(['box_id' => $box->id], 'box_id = 0');

            Sending::sendByActive('Вышла ' . $box->name);
        }
    }
}
