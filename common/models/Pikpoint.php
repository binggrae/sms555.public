<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pikpoint".
 *
 * @property integer $id
 * @property string $address
 * @property string $city
 * @property string $house
 * @property string $uid
 * @property string $name
 * @property string $region
 * @property string $street
 * @property integer $code
 */
class Pikpoint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pikpoint';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'city', 'house', 'uid', 'name', 'region', 'street', 'code'], 'required'],
            [['code'], 'integer'],
            [['address', 'city', 'house', 'uid', 'name', 'region', 'street'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Address',
            'city' => 'City',
            'house' => 'House',
            'uid' => 'Uid',
            'name' => 'Name',
            'region' => 'Region',
            'street' => 'Street',
            'code' => 'Code',
        ];
    }
}
