<?php

namespace common\models;

use common\components\Watcher;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "account_box".
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $box_id
 * @property integer $date
 * @property string $link
 * @property integer $is_delete
 * @property integer $is_order
 * @property integer $order_count
 * @property integer $order_id
 *
 * @property Account $account
 * @property Box $box
 */
class AccountBox extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_box';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'box_id', 'date'], 'required'],
            [['account_id', 'box_id', 'date', 'is_delete', 'is_delete', 'order_id'], 'integer'],
            [['link'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Аккаунт',
            'box_id' => 'Коробка',
            'date' => 'Дата',
            'link' => 'Ссылка',
            'is_order' => 'Заказ',
            'is_delete' => 'Is Delete',
        ];
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['id' => 'account_id']);
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->hasOne(Account::className(), ['account_id' => 'account_id']);
    }

    /**
     * @return Box
     */
    public function getBox()
    {
        return $this->hasOne(Box::className(), ['uid' => 'box_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->account->order_count++;
            $this->account->save(0);
        }

        return parent::beforeSave($insert);
    }


    /**
     * @return bool
     * @throws \Exception
     */
    public function drop()
    {
        $result = Watcher::get('http://elenakrygina.com/user/box/', $this->account->id);

        preg_match('/onclick=\"return false;\" order_id=\"([0-9a-z]+)\"/', $result, $match);
        $order_id = isset($match[1]) ? $match[1] : null;

        if ($order_id) {
            Watcher::post('http://elenakrygina.com/bitrix/tools/ajax/cancel_order.php',
                $this->account->id, [
                    'ID' => $order_id
                ]
            );
            $return = true;
        } else {
            $return = false;
        }
        $this->delete();

        return $return;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function check()
    {
        $result = Watcher::get('http://elenakrygina.com/user/box/', $this->account->id);

        preg_match('/onclick=\"return false;\" order_id=\"([0-9a-z]+)\"/', $result, $match);

        if (!isset($match[1])) {
            $this->delete();
            return false;
        } else {
            return true;
        }
    }


    /**
     * @return bool
     */
    public function order()
    {
        // calculate delivery price
        $result = Watcher::get('http://elenakrygina.com/box/personal/order/', $this->account->id);

        /** @var Order $order */

        $order = Order::find()
            ->where('account_id is null')
            ->andWhere('box_id = :id', [':id' => $this->box->id])
            ->andWhere(['oa_id' => $this->account_id])
            ->one();

        if (!$order) {
            $order = Order::find()
                ->where('account_id is null')
                ->andWhere('box_id = :id', [':id' => $this->box->id])
                ->andWhere('oa_id is null')
                ->one();
        }

        $data = Order::getOrderData($order,
            ArrayHelper::merge(Order::getOrderProperty($result), [
                'ORDER_PROP_2' => $this->account->login
            ])
        );

        $result = Watcher::post('http://elenakrygina.com/box/personal/order/',
            $this->account->id, $data
        );
        $result = mb_convert_encoding($result, "UTF-8", "windows-1251");

        // send order request
        $data = Order::getConfirmedData(
            ArrayHelper::merge($data, Order::getConfirmedProperty($result))
        );

        Watcher::post('http://elenakrygina.com/box/personal/order/',
            $this->account->id, $data
        );

        $this->is_order = 1;
        $this->save();

        if ($order) {
            $order->account_id = $this->account->id;
            $order->status = Order::STATUS_ORDER;
            $order->save();
        }
        return true;
    }

    /**
     * @return bool|int current order id
     */
    public function loadData()
    {

        $result = Watcher::get('http://elenakrygina.com/user/box/', $this->account->id);

        $exp = "/order_id=\"(\d+)\"/";

        preg_match($exp, $result, $match);

        if (count($match) == 2) {
            $this->order_id = $match[1];
            $return = $this->order_id;

            return $return;
        } else {
            $this->order_id = 0;
            $return = false;
        }
        $this->save(0);
        return $return;
    }




    // GETTERS QUERY

    /**
     *
     * @param bool $is_all
     * @return Query
     */
    public static function getOrderBox($is_all = false)
    {
        $query = AccountBox::find()->where(['is_order' => 1]);

        if (!$is_all) {
            $query
                ->leftJoin('order', 'account_box.account_id = order.account_id')
                ->andWhere('order.id is null');
        }

        return $query;
    }


}
