<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sms".
 *
 * @property integer $id
 * @property integer $sending_id
 * @property integer $created_at
 * @property integer $user_id
 * @property integer $sms_id
 * @property integer $status
 *
 * @property Sending $sending
 */
class Sms extends \yii\db\ActiveRecord
{

    const STATUS_WAIT = 0;
    const STATUS_DELETE = -1;
    const STATUS_INVALID = -2;
    const STATUS_NOT_DELIVERED = -3;
    const STATUS_CENTER = 1;
    const STATUS_DELIVERED = 2;
    const STATUS_NOT_FOUND = 10;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sending_id', 'created_at', 'user_id', 'sms_id', 'status'], 'required'],
            [['sending_id', 'created_at', 'user_id', 'sms_id', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Текст',
            'created_at' => 'Отправлено',
            'user_id' => 'Пользователь',
            'number' => 'Номер',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \common\models\Sending
     */
    public function getSending()
    {
        return $this->hasOne(Sending::className(), ['id' => 'sending_id']);
    }

    /**
     * @return \common\models\User
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function reformStatusCode($code)
    {
        switch ($code) {
            case $code == -3 :
                return self::STATUS_NOT_DELIVERED;
                break;
            case $code == -10 :
                return self::STATUS_WAIT;
                break;
            case $code == 3 :
                return self::STATUS_DELIVERED;
                break;
            case $code == -100 :
                return self::STATUS_NOT_FOUND;
                break;
            default :
                return $code;
        }
    }

    public function getStatusName()
    {
        $statuses = self::getStatusesArray();
        return isset($statuses[$this->status]) ? $statuses[$this->status] : '';
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_WAIT => 'В очереди',
            self::STATUS_DELETE => 'удалено',
            self::STATUS_INVALID => 'неверный адресат',
            self::STATUS_NOT_DELIVERED => 'сообщение не доставлено',
            self::STATUS_CENTER => 'передано в СМС центр',
            self::STATUS_DELIVERED => 'доставлено',
            self::STATUS_NOT_FOUND => 'сообщение не найдено',
        ];
    }
}
