<?php

namespace common\models;

use Yii;
use yii\web\HttpException;
use yii\db\Query;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $auth_key
 * @property string $phone
 * @property integer $role
 * @property integer $status
 * @property integer $is_active
 * @property integer $active_to
 * @property integer $created_at
 * @property integer $last_login
 * @property integer $test_count
 *
 * @property Profile $profile
 */
class User extends UserAuth implements IdentityInterface
{
    const ROLE_USER = 0;
    const ROLE_ADMIN = 1;

    const STATUS_GUEST = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_ACTIVE = 3;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auth_key', 'is_active', 'created_at', 'last_login'], 'required'],
            [['role', 'status', 'is_active', 'created_at', 'last_login', 'test_count'], 'integer'],
            [['auth_key'], 'string', 'max' => 32],
            [['phone'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auth_key' => 'Auth Key',
            'phone' => 'Телефон',
            'role' => 'Роль',
            'status' => 'Статус',
            'is_active' => 'Активен',
            'active_to' => 'Активен до',
            'created_at' => 'Дата создания',
            'last_login' => 'Последний вход',
            'test_count' => 'Тестовые смс',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }


    public static function login($user)
    {
        /** @var User $userModel */
        $userModel = self::findOne(['id' => $user->user_id]);
        if (!$userModel) {
            $userModel = self::register($user);
        }

        Yii::$app->user->login($userModel, 3600 * 24 * 30);
    }

    public static function register($user)
    {
        $data = Yii::$app->vk->getProfile($user);

        $userModel = new User([
            'id' => $user->user_id,
            'phone' => '',
            'status' => self::STATUS_GUEST,
            'role' => self::ROLE_USER,
            'is_active' => 0,
            'created_at' => time(),
            'last_login' => time(),
            'test_count' => 2,
        ]);
        $userModel->generateAuthKey();
        if ($userModel->save()) {
            $profileModel = new Profile([
                'user_id' => $user->user_id,
                'username' => $data->last_name . ' ' . $data->first_name,
                'first_name' => $data->first_name,
                'last_name' => $data->last_name,
                'photo' => $data->photo,
                'gender' => $data->sex,
                'city' => $data->city,
                'city_str' => '',
                'country' => $data->country,
                'country_str' => '',
            ]);
            if (!$profileModel->save()) {
                $userModel->delete();
                throw new HttpException(403, $userModel->getErrors());
            }

        } else {
            throw new HttpException(403, $userModel->getErrors());
        }

        return $userModel;

    }

    public static function visit()
    {

    }

    public function getStatusName()
    {
        $statuses = self::getStatusesArray();
        return isset($statuses[$this->status]) ? $statuses[$this->status] : '';
    }

    public static function getStatusesArray()
    {
        return [
            static::STATUS_GUEST => 'Не подтвердил соглашение',
            static::STATUS_INACTIVE => 'Не активен',
            static::STATUS_ACTIVE => 'Активен',
        ];
    }

    public function getRoleName()
    {
        $roles = self::getRolesArray();
        return isset($roles[$this->role]) ? $roles[$this->role] : '';
    }

    public static function getRolesArray()
    {
        return [
            self::ROLE_USER => 'Пользователь',
            self::ROLE_ADMIN => 'Администратор',
        ];
    }

    public function isGuest()
    {
        return Yii::$app->user->identity->status == User::STATUS_GUEST;
    }

    public function isActive()
    {
        return Yii::$app->user->identity->status == User::STATUS_ACTIVE;
    }

    public function getUsername()
    {
        return 1;
    }


    public function getActiveDay()
    {
        return abs(round(($this->active_to - time()) / 86400));
    }

    public function activate($period)
    {
        $this->active_to = $this->active_to > time() ? $this->active_to : time();
        $this->active_to += $period * 60 * 60 * 24;
        $this->status = User::STATUS_ACTIVE;
        $this->is_active = 1;
        $this->save();
        $active_to = date('d.m.Y', $this->active_to);

        Sending::send([$this->phone], "Услуга подключена на {$period} дней. Окончание: {$active_to}.");
    }

    public function deactivate()
    {
        $this->is_active = 0;
        $this->status = self::STATUS_INACTIVE;
        $this->save();

        Sending::send([$this->phone], "{$this->profile->first_name}, Ваша подписка закончилась.");
    }


    // GETTERS QUERY

    /**
     * @return Query
     */
    public static function getDeactivatedUser()
    {
        $query = User::find()
            ->where('active_to < :time', [':time' => time()])
            ->andWhere('status = :s', [':s' => User::STATUS_ACTIVE]);

        return $query;
    }

    /**
     * @return Query
     */
    public static function getNotifiedUser()
    {
        $query = User::find()
            ->where('active_to > :time_start', [':time_start' => time() + 3600 * 24,])
            ->andWhere(' active_to < :time_end', [':time_end' => time() + 3600 * 24 * 3,])
            ->andWhere(' status = :s', [':s' => User::STATUS_ACTIVE]);


        return $query;
    }


}
