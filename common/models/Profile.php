<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property integer $user_id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property integer $gender
 * @property integer $city
 * @property integer $country
 * @property string $city_str
 * @property string $country_str
 * @property string $photo
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'username', 'first_name', 'last_name', 'photo', 'gender', 'city', 'country'], 'required'],
            [['user_id', 'gender', 'city', 'country'], 'integer'],
            [['username', 'first_name', 'last_name', 'photo', 'city_str', 'country_str'], 'string', 'max' => 255],
            [['user_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'photo' => 'Фото',
            'username' => 'Логин',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'gender' => 'Пол',
            'city' => 'City',
            'country' => 'Country',
            'city_str' => 'Город',
            'country_str' => 'Страна',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
