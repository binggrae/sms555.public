<?php
return [
    'adminEmail' => 'admin@sms555.ru',
    'supportEmail' => 'support@sms555.ru',
    'user.passwordResetTokenExpire' => 3600,

    'is_verb' => 1,

];
