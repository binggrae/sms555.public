<?php
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('v2', dirname(dirname(__DIR__)) . '/v2');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
