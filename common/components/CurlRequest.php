<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 29.08.2015
 * Time: 15:12
 */

namespace v2\components;

use yii\base\Component;
use yii\base\Object;
use yii\helpers\VarDumper;

class CurlRequest extends Object
{
    protected $ch;

    public function __construct($link)
    {
        $this->ch = curl_init($link);
        parent::__construct([]);
    }

    public function setOpt($params)
    {
        curl_setopt_array($this->ch, $params);
    }

    /**
     * @return mixed
     */
    public function exec()
    {
        return curl_exec($this->ch);
    }

    /**
     * @return resource
     */
    public function getCh()
    {
        return $this->ch;
    }

    /**
     * @return mixed
     */
    public function execJson()
    {
        return json_decode(curl_exec($this->ch));
    }
}