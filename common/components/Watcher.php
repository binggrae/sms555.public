<?php
namespace common\components;


use common\models\Account;
use common\models\AccountBox;
use common\models\Box;
use common\models\Captcha;
use yii\base\Component;
use yii\helpers\VarDumper;

/**
 * @property Box $model
 */
class Watcher extends Component
{

    /** @var  Box */
    protected $model;


    public $id = null;

    public function init()
    {
        if ($this->id) {
            $this->model = Box::findOne(['uid' => $this->id]);
        } else {
            $this->model = new Box([
                'id' => null,
                'link' => 'http://elenakrygina.com/box/month-box'
            ]);
        }

        $this->model->loadAccount();

        $this->model->account->box_count++;
        $this->model->account->save(0);
    }


    public function base()
    {

        /** @var resource $ch */
        $ch = self::getCh($this->model->link, $this->model->account->id);

        $result = curl_exec($ch);
        $this->model->link = curl_getinfo($ch)['url'];
        curl_close($ch);

        $reg = '/id="detailItem" data-id="(\d+)"/';
        preg_match($reg, $result, $math);

        $this->model->id = $math[1];

        $reg = '/title>(.+)</';
        preg_match($reg, $result, $math);

        $title = $math[1];

        return [
            'id' => $this->model->id,
            'title' => $title,
            'link' => $this->model->link
        ];
    }

    public function check($attr)
    {
        if (!empty($attr)) {
            $this->model->id = $attr['id'];
            $this->model->link = $attr['link'];
        }
        $link = "http://elenakrygina.com/bitrix/templates/box_main/components/bitrix/catalog/main-box/bitrix/catalog.element/.default/script.php?id={$this->model->id}";
        $result = self::get($link, $this->model->account->id);

        $reg = ' /order":([0-9-])/';
        preg_match($reg, $result, $math);

        return $math[1];
    }


    public function order($attr)
    {
        if (!empty($attr)) {
            $this->model->id = $attr['id'];
            $this->model->link = $attr['link'];
        }

        /** @var Captcha $captcha */
//        while (1) {
//            $captcha = Captcha::find()
//                ->where('date > :time AND captcha.key != ""', [':time' => time() - 5400])
//                ->orderBy('id DESC')
//                ->one();
//
//            if ($captcha) {
//                break;
//            }
//            self::dump('no captcha');
//            sleep(1);
//        }

        $result = self::post('http://elenakrygina.com/bitrix/tools/ajax/box.add2cart.php',
            $this->model->account->id, [
                'ID' => $this->model->id,
                'QUANTITY' => 1,
//                'captcha_sid' => $captcha->code,
//                'captcha' => $captcha->key,
            ]
        );
//        $captcha->delete();

//  	var_dump($result);


        $data = json_decode($result);
        if (is_null($data)) {
            return false;
        }

        if (!isset($data->error)) {
            echo 'ORDER';
            $model = new AccountBox([
                'account_id' => $this->model->account->id,
                'box_id' => $this->model->id,
                'date' => time(),
                'link' => $this->model->link,
                'is_delete' => 0
            ]);
            if ($model->save()) {
                self::dump($model->getErrors());
            }
        } else {
            self::dump('' . $this->model->account->id);
            if ($data->error === 'Вы не авторизованы') {
                $this->model->account->is_login = 0;
                $this->model->save(0);
            }
            if ($data->message !== 'К сожалению, вы не успели, кто-то уже положил данный товар в корзину. Попробуйте положить товар в корзину позже, т.к. если товар не будет заказан в течение 30 минут, то он вернется в продажу. Если же товар будет заказан, но не оплачен в течение 2 суток, он также вернется в продажу.') {
                self::dump($data);
            }
        }
        return [
            'id' => $this->model->id,
            'link' => $this->model->link
        ];
    }

    public function deleteAccount()
    {


    }


    /**
     * @param string $link
     * @param integer $id
     * @return string
     */
    public static function get($link, $id = 0)
    {
        $ch = curl_init($link);
        curl_setopt_array($ch, [
            CURLOPT_INTERFACE => \Yii::$app->params['lan_interface'],
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36',
            CURLOPT_COOKIEJAR => \Yii::getAlias('@console/data/cookie/') . $id . '.txt',
            CURLOPT_COOKIEFILE => \Yii::getAlias('@console/data/cookie/') . $id . '.txt',
            CURLOPT_RETURNTRANSFER => 1,
        ]);
        $result = curl_exec($ch);
        curl_close($ch);
        return '' . str_replace("\r", '', str_replace("\n", '', $result));
    }

    /**
     * @param string $link
     * @param integer $id
     * @return resource
     */
    public static function getCh($link, $id = 0)
    {
        $ch = curl_init($link);
        curl_setopt_array($ch, [
            CURLOPT_INTERFACE => \Yii::$app->params['lan_interface'],
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36',
            CURLOPT_COOKIEJAR => \Yii::getAlias('@console/data/cookie/') . $id . '.txt',
            CURLOPT_COOKIEFILE => \Yii::getAlias('@console/data/cookie/') . $id . '.txt',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_HEADER => 1,
        ]);
        return $ch;
    }

    /**
     * @param string $link
     * @param integer $id
     * @param array $data
     * @return resource a cURL handle on success, false on errors.
     */
    public static function post($link, $id = 0, $data)
    {
        $ch = curl_init($link);
        curl_setopt_array($ch, [
            CURLOPT_INTERFACE => \Yii::$app->params['lan_interface'],
            CURLOPT_POST => 1,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_COOKIEJAR => \Yii::getAlias('@console/data/cookie/') . $id . '.txt',
            CURLOPT_COOKIEFILE => \Yii::getAlias('@console/data/cookie/') . $id . '.txt',
            CURLOPT_RETURNTRANSFER => 1,
        ]);

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public static function postVerb($link, $id = 0, $data)
    {
        $ch = curl_init($link);
        curl_setopt_array($ch, [
            CURLOPT_INTERFACE => \Yii::$app->params['lan_interface'],
//            CURLOPT_VERBOSE => 1,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [
                'Origin:http://elenakrygina.com',
                'Referer:http://elenakrygina.com/user/?login=yes'
            ],
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36',
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_COOKIEJAR => \Yii::getAlias('@console/data/cookie/') . $id . '.txt',
            CURLOPT_COOKIEFILE => \Yii::getAlias('@console/data/cookie/') . $id . '.txt',
            CURLOPT_RETURNTRANSFER => 1,
        ]);


        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public static function dump($data)
    {
        if (\Yii::$app->params['is_verb']) {
            if (is_string($data)) {
                echo date('H:i:s - ') . $data . "\n";
            } else {
                var_dump($data);
            }
        }
    }


}