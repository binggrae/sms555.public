<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 15.12.2015
 * Time: 18:53
 */

namespace common\components;


// класс запроса
use yii\helpers\VarDumper;

class Curl
{
    // свойства
    var $link;
    var $headers;
    var $chunked = false;
    var $response = '';

    // метод запроса
    function GetRequest()
    {
        // ссылки свойств
        $link = &$this->link;
        $headers = &$this->headers;
        $chunked = &$this->chunked;
        $response = &$this->response;
        // логика метода
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        if ($chunked) {
            curl_setopt($ch, CURLOPT_WRITEFUNCTION, array($this, 'Callback'));
            curl_exec($ch);
        } else {
            $response = curl_exec($ch);
        };
        curl_close($ch);
        $response = html_entity_decode($response, null, 'UTF-8');
        return $response;
    }

    // метод callback запроса
    function Callback($ch, $str)
    {
        // ссылки свойств
        $response = &$this->response;
        // логика метода
        $response .= $str;
        return strlen($str);
    }
}

