<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 29.08.2015
 * Time: 15:02
 */

namespace v2\components;


use yii\base\Component;
use yii\helpers\ArrayHelper;

class Curl extends Component
{

    public function get($link, $data = [], $params = [])
    {
        if (!empty($data)) {
            $link = $link . '?' . urldecode(http_build_query($data));
        }
        $params = ArrayHelper::merge([
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36',
            CURLOPT_RETURNTRANSFER => 1,
        ], $params);

        $request = new CurlRequest($link);
        $request->setOpt($params);

        return $request;

    }

    public function post($link, $data = [], $params = [])
    {
        $params = ArrayHelper::merge([
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36',
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
        ], $params);

        $request = new CurlRequest($link);
        $request->setOpt($params);

        return $request;
    }


}