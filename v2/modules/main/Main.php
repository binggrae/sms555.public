<?php

namespace v2\modules\main;

class Main extends \yii\base\Module
{
    public $controllerNamespace = 'v2\modules\main\controllers';

    public function init()
    {
        parent::init();

        $this->layoutPath = \Yii::getAlias('@v2/modules/main/views/layouts');
        $this->layout = 'main';

    }
}
