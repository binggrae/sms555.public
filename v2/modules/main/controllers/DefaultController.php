<?php

namespace v2\modules\main\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $this->layout = 'home';
        return $this->render('index');
    }


    public function actionNews()
    {
        return $this->render('news');

    }
}
