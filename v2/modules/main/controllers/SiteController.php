<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 29.08.2015
 * Time: 13:24
 */

namespace v2\modules\main\controllers;


use yii\web\Controller;

class SiteController extends Controller {

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = 'home';
        return $this->render('index');
    }
}

