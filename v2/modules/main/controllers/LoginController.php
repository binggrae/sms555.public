<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 29.08.2015
 * Time: 13:27
 */

namespace v2\modules\main\controllers;

use Yii;
use common\models\User;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;

class LoginController extends Controller
{

    public function actionIndex()
    {
        $params = [
            'client_id' => \Yii::$app->params['vk_client_id'],
            'redirect_uri' => 'http://' . \Yii::$app->request->serverName . Url::to('/main/login/auth'),
            'scope' => 'email',
            'response_type' => 'code'
        ];

        return $this->redirect('https://oauth.vk.com/authorize?' . urldecode(http_build_query($params)));
    }

    public function actionAuth($code = null, $error = null)
    {
        if ($error) {
            return $this->goHome();
        }

        $user = \Yii::$app->vk->auth($code);

        if (isset($user->user_id)) {
            User::login($user);
        }
        return $this->redirect(['/cabinet/default/index']);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionRegister()
    {
        /* @var $model User */
        $model = User::findOne(['id' => Yii::$app->user->id]);

        if ($model->load(Yii::$app->request->post())) {
            $model->status = User::STATUS_INACTIVE;
            if ($model->save()) {
                $this->redirect('/cabinet');
            }
        }

        return $this->render('register', ['model' => $model]);
    }
}