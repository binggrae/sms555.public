<?php
use yii\helpers\Url;
/* @var $this \yii\web\View */
?>
<div class="row">
    <div class="col-md-9">
        <?=$this->render('_menu'); ?>
    </div>
    <div class="col-md-3">
        <div class="sign">
            <div class="block">
                <a href="<?= Url::to(['/cabinet/default/index']); ?>" class="btn btn-primary">
                    <i class="fa fa-sign-in"></i>
                    Личный кабинет
                </a>
            </div>
            <div class="link">
                <?php if (Yii::$app->user->isGuest) : ?>
                    <a href="<?= Url::to(['/main/login/index']); ?>">Вход / регистрация</a>
                <?php else : ?>
                    <a href="<?= Url::to(['/main/login/logout']); ?>">Выйти</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>