<?php

use common\models\User;
use \yii\widgets\Menu;

/* @var $this \yii\web\View */
echo Menu::widget([
    'options' => ['class' => 'menu navbar-nav nav'],
    'items' => [
        ['label' => 'Главная', 'url' => ['/main/default/index']],
        ['label' => 'Тарифы', 'url' => ['/main/default/index', '#' => 'tarifi']],
        ['label' => 'Преимущества сервиса', 'url' => ['/main/default/index', '#' => 'preimusestva']],
        ['label' => 'Способы оплаты', 'url' => ['/main/default/index', '#' => 'sposobi-oplati']],
        //['label' => 'Новости', 'url' => ['/main/default/news']],
        ['label' => 'Админ', 'url' => '/',
            'template' => '<a class="dropdown-toggle" href="{url}" data-toggle="dropdown"> ' .
                '{label}<b class="caret"></b></a>',
            'submenuTemplate' => "\n<ul class='dropdown-menu'>\n{items}\n</ul>\n",
            'options' => ['class' => 'dropdown'],
            'items' => [
                ['label' => 'Пользователи', 'url' => ['/admin/user/index']],
                ['label' => 'Смс', 'url' => ['/admin/sms/index']],
                ['label' => 'Оплаты', 'url' => ['/admin/transaction/index']],
                ['label' => 'Аккаунты', 'url' => ['/admin/account/index']],
                ['label' => 'Коробки', 'url' => ['/admin/account-box/index']],
                ['label' => 'Заказы', 'url' => ['/admin/order/index']],
                ['label' => 'Архив', 'url' => ['/admin/order/history']],
            ],
            'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->role == User::ROLE_ADMIN
        ]
    ],
]);