<?php
use app\components\Stat;

/* @var $this \yii\web\View */
?>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <ul>
                <li><a href="#" data-toggle="modal" data-target="#rules">Правила сайта</a></li>
                <li><a href="#" data-toggle="modal" data-target="#conf">Политика конфиденциальности</a></li>
            </ul>
            <div style="font-size: 14px;">© 2015. <a href="/">sms555.ru</a></div>
        </div>
        <div class="col-md-4">
            <ul>
                <li>vk.com: <a href="//vk.com/id335304943" target="_blank">vk.com/id335304943</a></li>
                <li>email: <a href="mailto:sms555ru@gmail.com">sms555ru@gmail.com</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="modal fade" id="rules">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title">Правила сайта</h3>
            </div>
            <div class="modal-body">
                <?= $this->render('_rules'); ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="conf">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title">Политика конфиденциальности</h3>
            </div>
            <div class="modal-body">
                <p>Сайт <a href="/">sms555.ru</a> уважает и соблюдает законодательство РФ. Также мы уважаем Ваше право и
                    соблюдаем
                    конфиденциальность при заполнении, передаче и хранении ваших конфиденциальных сведений.
                </p>

                <p>Мы запрашиваем Ваши персональные данные исключительно для информирования об оказываемых услугах
                    сайта.
                </p>

                <p>Персональные данные - это информация, относящаяся к субъекту персональных данных, то есть, к
                    потенциальному покупателю. В частности, это фамилия, имя и отчество, дата рождения, адрес,
                    контактные реквизиты (телефон, адрес электронной почты), семейное, имущественное положение и иные
                    данные, относимые Федеральным законом от 27 июля 2006 года № 152-ФЗ «О персональных данных» (далее –
                    «Закон») к категории персональных данных.
                </p>

                <p>Ваша конфиденциальность очень важна для нас. Мы против спама и за улучшение качества интернета,
                    поэтому личная информация, собранная при регистрации (или в любое другое время) будет использована
                    только для отправки нашей информации и не будет передана или продана третьим сторонам.</p>

                <p>В случае отзыва согласия на обработку своих персональных данных мы обязуемся удалить Ваши
                    персональные данные в срок не позднее 3 рабочих дней.
                </p>
            </div>
        </div>
    </div>
</div>
<div>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter30065264 = new Ya.Metrika({
                        id: 30065264,
                        webvisor: true,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true
                    });
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="//mc.yandex.ru/watch/30065264" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
</div>


