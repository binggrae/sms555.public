<?php

use v2\assets\HomeAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

HomeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<a href="#" class="go-top"></a>

<section class="section-1">
    <div class="container">
        <?= $this->render('_head'); ?>
    </div>
    <div class="line"></div>
</section>
<section class="page">
    <div class="container">
        <?= $content; ?>
    </div>
</section>
<section class="section-7">
    <?= $this->render('_footer'); ?>
</section>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
