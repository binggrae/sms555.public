<?php
use v2\assets\HomeAsset;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

HomeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<a href="#" class="go-top"></a>

<!------------- HEAD-->
<section class="section-1">
    <div class="container">
        <?= $this->render('_head'); ?>
    </div>
    <div class="line"></div>
</section>

<!------------- HOME white-->
<section class="section-2">
    <div class="container">
        <div class="row">
            <h2>Приветствуем Вас на уникальном ресурсе «SMS-информирования»!</h2>
			
			
			
			<p>Наверняка Вы ни раз задавались вопросом, как же успеть купить замечательные коробочки KryginaBox? Как же узнать когда будет старт
                продаж? Неужели нужно судорожно, круглосуточно сидеть в интернете, чтобы заполучить заветную коробочку?</p>

            <h3>Все! Решение найдено!</h3>

            <p>Теперь не надо целыми днями «залипать» возле экрана монитора и отвлекаться от важных дел!</p>
			

			
			<h3>Как это работает?</h3>

            <p>1) Вы просто подключаетесь (регистрируетесь) к сервису «SMS-информирования».</p>

            <p>2) Оплачиваете пакет sms-уведомлений.</p>

            <br/>

            <p>И все! Как только появится новая KryginaBox - сервис
                «SMS-информирования» Вам отправит sms-уведомление.</p>

            <p><strong>Внимание!</strong> Не забудте включить мобильный телефон!</p>


            <div class="arrow">
                <h3 class="light">Готовы начать работу?</h3>

                <a class="btn btn-primary" href="<?= Url::to(['/main/login/index']); ?>">ПОДКЛЮЧИТЬСЯ</a>
            </div>

            <p class="text-center">Если остались вопросы, свяжитесь с нами по E-email:
                <a href="mailto:sms555ru@gmail.com">sms555ru@gmail.com</a> или
                <a href="//vk.com/id335304943" target="_blank">ВКонтакте</a>
            </p>
        </div>
    </div>
</section>

<!------------- PRICE blue-->
<section class="section-3" id="tarifi">
    <div class="container">
        <div class="row">
            <h1>Тарифы</h1>

            <p>С сервисом «SMS-информирования» Вы всегда будете в курсе о выходе новой KryginaBox</p>

            <p>Наш сервис обладает наиболее простым и удобным способом оплаты: зарегистрируйтесь на
                сервисе и пополните свой баланс в личном кабинете. Стоимость указывается в российских рублях с учетом
                всех налогов.</p>

            <br/>

            <div class="row pricing">
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="head"></div>
                    <div class="body">
                        <ul class="link">
                            <li data-toggle="tooltip">
                                <span data-toggle="tooltip" data-placement="top"
                                      title="Уведомление о старте продаж коробочек красоты. Если в течении оплаченого периода выйдет несколько коробочек, то уведомление придёт о каждой из них">SMS о выходе новых коробочек</span>
                            </li>
                            <li>
                                <span data-toggle="tooltip" data-placement="top"
                                      title="30/90 календарных дней с момента оплаты. Отключение пакета происходит в 0:00 часов по московскому времени">Период</span>
                            </li>
                            <li class="even">
                                <span data-toggle="tooltip" data-placement="top"
                                      title="Комисси нет. Скрытых платежей нет. ">Стоимость</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6  col-xs-6">
                    <div class="head">
                        <div class="name">
                            мини
                        </div>
                    </div>
                    <div class="body">
                        <ul>
                            <li><img src="/img/icon_green_ok.png" alt=""/></li>
                            <li>1 месяц</li>
                            <li class="even">100 рублей</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6 big_hide">
                    <div class="head"></div>
                    <div class="body">
                        <ul class="link">
                            <li data-toggle="tooltip">
                                <span data-toggle="tooltip" data-placement="top"
                                      title="Уведомление о старте продаж коробочек красоты. Если в течении оплаченого периода выйдет несколько коробочек, то уведомление придёт о каждой из них">SMS о выходе новых коробочек</span>
                            </li>
                            <li>
                                <span data-toggle="tooltip" data-placement="top"
                                      title="30/90 календарных дней с момента оплаты. Отключение пакета происходит в 0:00 часов по московскому времени">Период</span>
                            </li>
                            <li class="even">
                                <span data-toggle="tooltip" data-placement="top"
                                      title="Комисси нет. Скрытых платежей нет. ">Стоимость</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6  col-xs-6">
                    <div class="head">
                        <div class="name">
                            макси
                        </div>
                    </div>
                    <div class="body">
                        <ul>
                            <li><img src="/img/icon_green_ok.png" alt=""/></li>
                            <li>3 месяца</li>
                            <li class="even">250 рублей</li>
                        </ul>
                    </div>
                </div>
            </div>

            <h3 class="text-center">Используя преимущества Сервиса «SMS-информирования», <br>
                Вы первыми узнаете о выходе новой KryginaBox!</h3>


            <div class="arrow">
                <h3 class="light">Готовы начать работу?</h3>

                <a class="btn btn-primary" href="<?= Url::to(['/main/login/index']); ?>">ПОДКЛЮЧИТЬСЯ</a>
            </div>

            <p class="text-center">Если остались вопросы, свяжитесь с нами по E-email:
                <a href="mailto:sms555ru@gmail.com">sms555ru@gmail.com</a> или
                <a href="//vk.com/id335304943" target="_blank">ВКонтакте</a>
            </p>
        </div>
    </div>
</section>

<!------------- JOB white-->
<section class="section-4">
    <div class="container">
        <div class="row">
            <h1>Принципы работы.</h1>

            <p>Данный сервис <b>только</b> информирует Вас о появлении новых коробочек на сайте Елены Крыгиной.</p>

            <p>С подключением данного сервиса у Вас будет больше шансов успеть купить коробочку, т.к. Вы раньше других
                узнаете о старте продаж.</p>

            <p>Данный сервис не предназначен для перепродажи коробочек, и у него нет возможности положить товар в
                корзину
                за Вас!</p>


            <p></p>

            <div class="arrow">
                <h3 class="light">Готовы начать работу?</h3>

                <a class="btn btn-primary" href="<?= Url::to(['/main/login/index']); ?>">ПОДКЛЮЧИТЬСЯ</a>
            </div>

            <p class="text-center">Если остались вопросы, свяжитесь с нами по E-email:
                <a href="mailto:sms555ru@gmail.com">sms555ru@gmail.com</a> или
                <a href="//vk.com/id335304943" target="_blank">ВКонтакте</a>
            </p>

        </div>
    </div>
</section>

<!------------- PREIMU blue-->
<section class="section-5" id="preimusestva">
    <div class="container">
        <div class="row">
            <h1>Преимущества сервиса <br/> «SMS-информирования»</h1>

            <div class="row">
                <div class="col-sm-3"></div>
                <div class="rect col-sm-6">
                    <div class="head">
                        <img src="/img/like-up.png" alt=""/>

                        <h2>7 бесценных преимуществ Сервиса:</h2>

                    </div>
                    <div class="line"></div>
                    <div class="body">
                        <ul>
                            <li>
                                <div class="marker">1</div>
                                Позволяет Вам первыми узнать о выходе новой KryginaBox.
                            </li>
                            <li>
                                <div class="marker">2</div>
                                Бережет Ваши нервы.
                            </li>
                            <li>
                                <div class="marker">3</div>
                                Экономит Ваше время.
                            </li>
                            <li>
                                <div class="marker">4</div>
                                Прост в использовании.
                            </li>
                            <li>
                                <div class="marker">5</div>
                                Надежность и конфиденциальность.
                            </li>
                            <li>
                                <div class="marker">6</div>
                                Доступность стоимости Сервиса.
                            </li>
                            <li>
                                <div class="marker">7</div>
                                Гарантия качества 100%. Гарантированным результатом можно считать обязательность
                                доставки sms-сообщения.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3"></div>
            </div>

            <h3 class="text-center">Экономьте своё время!</h3>

            <div class="arrow">
                <h3 class="light">Готовы начать работу?</h3>

                <a class="btn btn-primary" href="<?= Url::to(['/main/login/index']); ?>">ПОДКЛЮЧИТЬСЯ</a>
            </div>

            <p class="text-center">Если остались вопросы, свяжитесь с нами по E-email:
                <a href="mailto:sms555ru@gmail.com">sms555ru@gmail.com</a> или
                <a href="//vk.com/id335304943" target="_blank">ВКонтакте</a>
            </p>
        </div>
    </div>
</section>

<!------------- OPLATA white-->
<section class="section-6" id="sposobi-oplati">
    <div class="container">
        <div class="row">
            <h1>Способы оплаты</h1>

            <h2 class="text-center">Яндекс.Деньги</h2>

            <p>Оплата услуг осуществляется на сайте Яндекс.Денег</p>

            <p><strong>Яндекс.Деньги</strong> — это сервис онлайн-платежей, который работает 24 часа в сутки и 7 дней в
                неделю. Пользоваться Яндекс.Деньгами легко. Все интерфейсы — сайт, его мобильная версия, приложения для
                смартфонов — простые и удобные.</p>

            <p>Чтобы открыть кошелек, нужна всего пара минут. Следующий шаг — пополнение (например, наличными). Кроме
                того, к счету можно привязать банковскую карту, чтобы платить прямо с нее: тогда не придется указывать
                реквизиты карты в интернете.</p>

            <p>Платежи Яндекс.Деньгами выполняются мгновенно — в любое время дня и ночи.</p>

            <h2 class="text-center">Карты VISA, MasterCard и другие</h2>

            <p>Оплата услуг осуществляется на сайте Яндекс.Денег</p>

            <p>При оплате банковской картой (включая ввод номера карты), обработка платежа происходит на сайте системы
                электронных платежей Яндекс.Деньги, которая прошла международную сертификацию. Это значит, что Ваши
                конфиденциальные данные (реквизиты карты, регистрационные данные и др.) и их обработка полностью
                защищена и никто, в том числе www.sms555.ru, не может получить персональные и банковские данные
                клиента.</p>

            <p></p>

            <div class="arrow">
                <h3 class="light">Готовы начать работу?</h3>

                <a class="btn btn-primary" href="<?= Url::to(['/main/login/index']); ?>">ПОДКЛЮЧИТЬСЯ</a>
            </div>

            <p class="text-center">Если остались вопросы, свяжитесь с нами по E-email:
                <a href="mailto:sms555ru@gmail.com">sms555ru@gmail.com</a> или
                <a href="//vk.com/id335304943" target="_blank">ВКонтакте</a>
            </p>

        </div>
    </div>
</section>

<!------------- FOOTER black-->
<section class="section-7">
    <?= $this->render('_footer'); ?>
</section>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
