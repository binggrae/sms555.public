<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */

/* @var $form yii\widgets\ActiveForm */

$this->title = 'Регистрация  | sms555.ru';
?>

<h2>Регистрация</h2>

<div class="login-register">
    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'phone')
        ->hint('Телефон можно указать позже в личном кабинете')
        ->widget(MaskedInput::className(), [
            'name' => 'phone',
            'mask' => '+7 (999) 999 - 9999',
        ]) ?>

    <div class="form-group">
        <div class="control-label"><b>Правила</b></div>
        <div class="rules">
            <?= $this->render('/layouts/_rules'); ?>
        </div>
    </div>

    <div class="form-group">
        <p><i>Регистрируясь, вы принимаете соглашение</i></p>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>