<?php

namespace v2\modules\cabinet\controllers;


use common\models\Transaction;
use v2\models\TransactionForm;
use v2\modules\cabinet\components\Controller;
use yii\data\ActiveDataProvider;

class PaymentController extends Controller
{
    public function actionIndex()
    {
        $model = new TransactionForm();
        return $this->render('index', ['model' => $model]);
    }


    public function actionHistory()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Transaction::find()->where(['user_id' => \Yii::$app->user->id])->orderBy('id DESC'),
        ]);
        return $this->render('history', ['dataProvider' => $dataProvider]);
    }

}