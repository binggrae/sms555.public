<?php

namespace v2\modules\cabinet\controllers;

use common\models\User;
use v2\modules\cabinet\components\Controller;


class DefaultController extends Controller
{

    public function actionIndex()
    {
        /* @var $model User */
        $model = User::findOne(['id' => \Yii::$app->user->id]);

        return $this->render('index', ['model' => $model]);
    }


    public function actionSettings()
    {
        /* @var $model User */
        $model = User::findOne(['id' => \Yii::$app->user->id]);
		
		if ($model->load(\Yii::$app->request->post())) {
            if ($model->save()) {
                $this->refresh();
            }
        }

        return $this->render('settings', ['model' => $model]);
    }
}
