<?php

namespace v2\modules\cabinet\controllers;

use common\models\Sending;
use common\models\Sms;
use common\models\User;
use v2\modules\cabinet\components\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

class SmsController extends Controller
{

    public function actionSend()
    {
        /* @var $model User */
        $model = User::findOne(['id' => \Yii::$app->user->id]);

        if ($model->test_count > 0) {
            $model->test_count--;
            if ($model->save()) {
                if(Sending::send([$model->phone], 'Здравствуйте! Это тестовая смс с сайта sms555.ru')) {
                    \Yii::$app->session->setFlash('sms_send', 'Тестовое смс отправлено. Осталось: ' . $model->test_count);
                }
            };
        } else {
            \Yii::$app->session->setFlash('error', 'Ошибка отправления смс. Превышен лимит тестовых смс');
        }
        return $this->redirect('/cabinet/sms/index');
    }


    public function actionIndex()
    {
        return $this->render('sms');
    }


    public function actionHistory()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Sms::find()->where(['user_id' => \Yii::$app->user->id])->orderBy('id DESC'),
        ]);
        return $this->render('history', [
            'dataProvider' => $dataProvider
        ]);
    }

}