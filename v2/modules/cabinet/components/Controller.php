<?php

namespace v2\modules\cabinet\components;

use common\models\User;

class Controller extends \yii\web\Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            /* @var $user User */
            $user = \Yii::$app->user->identity;
            if(\Yii::$app->user->isGuest) {
                $this->redirect(['/main/login/index']);
                return false;
            } else {
                $user->visit();
            }
            if($user->isGuest()) {
                $this->redirect(['/main/login/register']);
                return false;
            }

            $user->last_login = time();
            $user->save(0);

            return true;
        } else {
            return false;
        }
    }
}
