<?php

use common\models\Transaction;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;


/* @var $this yii\web\View */
/* @var $model v2\models\TransactionForm */


$this->title = 'Оплата';
$js = <<<EOT
$(document).ready(function() {
    $('.item').on('click', function() {
        $('.method').show();
        $('.item').removeClass('active');
        $(this).addClass('active');
        $('#transactionform-period').val($(this).find('.period').data('count'));

    });
});
EOT;
$this->registerJs($js, View::POS_END);
?>

<div class="row payment">
    <div class="col-md-3 col-md-push-9">
        <?= $this->render('../layouts/_menu'); ?>
    </div>
    <div class="col-md-9 col-md-pull-3">
        <h2>Оплата</h2>

        <div class="activate-form">
            <?php $form = ActiveForm::begin([
                'action' => Url::to('/transaction/default/run')
            ]); ?>

            <div class="row">
                <div class="col-md-12">
                    <h4>SMS-уведомления о выходе новых коробочек KryginaBox</h4>
                </div>
                <div class="col-md-4 item">
                    <div class="wrapper">
                        <p>30 дней</p>

                        <p>100 руб.</p>
                        <span class="period btn btn-primary" data-count="30">Оплатить</span>
                        <div class="shadow"></div>
                    </div>

                </div>
                <div class="col-md-4 item">
                    <div class="wrapper">
                        <p>90 дней</p>

                        <p>250 руб.</p>
                        <span class="period btn btn-primary" data-count="90">Оплатить</span>
                        <div class="shadow"></div>
                    </div>
                </div>
            </div>

            <?= $form->field($model, 'period', ['template' => '{input}'])->hiddenInput(); ?>

            <hr/>

            <div class="row method">
                <div class="col-md-12">
                    <h4>Способ оплаты</h4>

                    <?= $form->field($model, 'type', ['template' => '{input}'])->radioList(Transaction::getPaymentArray())
                        ->hint('С счета спишется указанная сумма, без дополнительных комиссий.') ?>

                    <?= Html::submitButton('Оплатить', ['class' => 'btn btn-primary']); ?>

                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>


    </div>
</div>
