<?php
use common\models\Transaction;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'История платежей  | sms555.ru';
?>

<div class="row">
    <div class="col-md-3 col-md-push-9">
        <?= $this->render('../layouts/_menu'); ?>
    </div>
    <div class="col-md-9 col-md-pull-3">
        <h2>Ваши платежи</h2>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                [
                    'attribute' => 'period',
                    'value' => function ($data) {
                        return $data->period . ' Дней';
                    }
                ],
                'count',
                [
                    'attribute' => 'service',
                    'value' => function ($data) {
                        return Transaction::getServiceArray()[$data->service];
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'format' => 'html',
                    'value' => function ($data) {
                        return date('d.m.Y', $data->created_at) . ' <small>' .
                        date('H:i:s', $data->created_at) . '</small>';
                    }
                ],
            ],
        ]); ?>
    </div>
</div>
