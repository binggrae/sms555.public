<?php
use yii\bootstrap\Nav;

?>
    <h3>Меню</h3>
<?php echo Nav::widget([
    'options' => ['class' => 'nav-pills nav-stacked'],
    'items' => [
        ['label' => 'Кабинет', 'url' => ['/cabinet/default/index']],
        ['label' => 'Настройки', 'url' => ['/cabinet/default/settings']],
        ['label' => 'Оплата', 'url' => ['/cabinet/payment/index']],
        ['label' => 'История платежей', 'url' => ['/cabinet/payment/history']],
        ['label' => 'Тестирование смс', 'url' => ['/cabinet/sms/index']],
        ['label' => 'История смс', 'url' => ['/cabinet/sms/history']],
        ['label' => 'Выход', 'url' => ['/main/login/logout']],
    ],
]);
