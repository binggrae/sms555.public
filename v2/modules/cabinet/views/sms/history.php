<?php

use common\models\Sms;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'История смс | sms555.ru';
?>


<div class="row">
    <div class="col-md-3 col-md-push-9">
        <?= $this->render('../layouts/_menu'); ?>
    </div>
    <div class="col-md-9 col-md-pull-3">
        <h2>Ваши смс</h2>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'message',
                    'format' => 'html',
                    'value' => function ($data) {
                        /** @var $data Sms */
                        return $data->sending->message;
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'format' => 'html',
                    'value' => function ($data) {
                        return date('d.m.Y', $data->created_at) . ' <small>' .
                        date('H:i', $data->created_at) . '</small>';
                    },
                ],
                [
                    'attribute' => 'status',
                    'value' => function ($data) {
                        return Sms::getStatusesArray()[$data->status];
                    }
                ],
            ],
        ]); ?>
    </div>
</div>
