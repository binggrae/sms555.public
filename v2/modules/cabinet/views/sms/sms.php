<?php


use common\widgets\Alert;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/* @var $this yii\web\View */

$this->title = 'Тестирование SMS | sms555.ru';
?>
<div class="row">
    <div class="col-md-3 col-md-push-9">
        <?= $this->render('../layouts/_menu'); ?>
    </div>
    <div class="col-md-9 col-md-pull-3">
        <h2>Тестирование SMS</h2>

        <?php echo Alert::widget(); ?>


        <?php if (!Yii::$app->user->identity->phone) : ?>
            <p>Для тестирования нужно указать номер мобильного телефона.
                <a href="<?= Url::to(['cabinet/default/settings']); ?>">Перейти</a>
            </p>
        <?php else : ?>

            <p>Вы можете протестировать входящие смс</p>

            <p>Номер телефона: <b><?= Yii::$app->user->identity->phone; ?></b></p>

            <p>Количество тестовых смс: <b><?= Yii::$app->user->identity->test_count; ?></b></p>

            <?php if (Yii::$app->user->identity->test_count > 0) : ?>
                <a href="<?= Url::to('/cabinet/sms/send'); ?>" class="btn btn-primary">Отправить SMS</a>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
