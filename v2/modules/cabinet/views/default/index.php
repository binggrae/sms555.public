<?php
use app\models\Package;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\User */


$this->title = 'Личный кабинет | sms555.ru';
?>

<div class="row">
    <div class="col-md-3 col-md-push-9">
        <?= $this->render('../layouts/_menu'); ?>
    </div>
    <div class="col-md-9 col-md-pull-3">
        <h2>Личный кабинет</h2>

        <?php if (!$model->phone) : ?>
            <div class="alert alert-danger" role="alert">
                Вы не указали номер телефона. Для того чтобы мы смогли отправить Вам смс, требуется указать его в <a
                    href="<?= Url::to('/cabinet/default/settings'); ?>">настройках</a>.
            </div>
        <?php endif; ?>

        <div>
            Ваш статус: <?= $model->getStatusName(); ?>
            <?php if ($model->isActive()) : ?>
                <span> до <?= date('d.m.Y', $model->active_to); ?>.</span>
                <br/><br/>

                <?php if ($model->active_to - time() < 86400 * 3) : ?>
                    <div class="alert alert-warning" role="alert">
                        <p>Ваша подписка заканчивается
                            <?= \Yii::t('app', '{n, plural, =0{сегодня} =1{через # день} other{через # дня}}', [
                                'n' => $model->getActiveDay()
                            ]); ?>
                        </p>
                        <p>Для оплаты перейдите по <a href="<?= Url::to('/cabinet/payment/index'); ?>">ссылке</a>.</p>
                    </div>
                <?php endif; ?>

            <?php else : ?>
                <p>Для оплаты перейдите по <a href="<?= Url::to('/cabinet/payment/index'); ?>">ссылке</a>.</p>
            <?php endif; ?>
        </div>
    </div>
</div>
