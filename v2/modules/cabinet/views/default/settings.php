<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Настройки  | sms555.ru';
?>
<div class="row">
    <div class="col-md-3 col-md-push-9">
        <?= $this->render('../layouts/_menu'); ?>
    </div>
    <div class="col-md-9 col-md-pull-3">
        <h3>Настройки</h3>

        <p>Здесь вы можете сменить номер телефона, на который будут приходить уведомления.</p>

        <div class="profile-form">

            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
                        'name' => 'phone',
                        'mask' => '+7 (999) 999 - 9999'
                    ]) ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
