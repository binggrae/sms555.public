<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Account */

$this->title = $model->login;
$this->params['breadcrumbs'][] = ['label' => 'Аккаунты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id',
                'format' => 'raw',
                'value' =>
                    Html::a(
                        $model->id,
                        Url::to(['/admin/account/proxy', 'id' => $model->id]),
                        ['target' => '_blank']

                    )
            ],
            'login',
            'password',
            'mail_password',
        ],
    ]) ?>

   <div class="col-md-6">
       <h5>Текст для первого письма</h5>
       <blockquote>
           <p>Реквизиты карты СБ: 4276 8720 9141 9201 Держатель карты, Надежда Андреевна П.</p>

           <p>Яндекс кошелёк: 41001894137683</p>

           <p>Qiwi: 8 (982) 290-05-44</p>

           <p>В комментарии к платежу укажите фамилию того, на кого оформлялась коробочка</p>
       </blockquote>
       <h5>Текст для второго письма</h5>
       <blockquote>
           <p>Реквизиты для сайты Крыгиной</p>

           <p>Логин: <?= $model->login; ?></p>

           <p>Пароль: <?= $model->password; ?></p><br/>

           <p>Реквизиты для входа в почту</p>

           <p>Логин: <?= $model->login; ?></p>

           <p>Пароль: <?= $model->mail_password; ?></p><br/>

           <p>Рекомендуем изменить пароли от почты и сайта Крыгиной</p>

           <p>Внимание, в корзине коробочки нет, она находится в личном кабинете! Для того, чтобы ее найти,
               посмотрите скриншот, который прикреплен к этому письму. Следуйте указаниям и у вас все получится</p>
       </blockquote>
   </div>

</div>
