<?php


/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/* @var $searchModel v2\modules\admin\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//yii\helpers\Html::tag('a', $data->login, ['href' => yii\helpers\Url::to(['view', ['id' => $data->id]])])
$this->title = 'Аккаунты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-index">


	<?= Html::a('Привязка', Url::to(['/admin/account/phone'])); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'login',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    return Html::tag('a', $model->{$column->attribute}, ['href' => Url::to(['view', 'id' => $model->id])]);
                }
            ],
            'password',
            'mail_password',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}'
            ],
        ],
    ]); ?>

</div>
