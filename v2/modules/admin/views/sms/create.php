<?php

use v2\modules\admin\models\ManualSending;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\Sending */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Создать рассылку';
$this->params['breadcrumbs'][] = ['label' => 'SMS', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="box-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'from')->dropDownList(ManualSending::getFromLabel()) ?>

        <?= $form->field($model, 'message')->textInput() ?>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
