<?php

use common\models\Sending;
use common\models\Sms;
use common\models\User;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Sending */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->id . " # " . date('d.m.Y H:i:s', $model->created_at);
$this->params['breadcrumbs'][] = ['label' => 'SMS', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sending-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <br/>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'created_at',
                'format' => 'html',
                'value' => date('d.m.Y', $model->created_at) . ' <small>' .
                    date('H:i:s', $model->created_at) . '</small>'
            ],
            [
                'attribute' => 'status',
                'value' => Sending::getStatusesArray()[$model->status]
            ],
            'message:ntext',
        ],
    ]) ?>

    <hr/>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'user.profile.photo',
                'header' => '',
                'format' => 'html',
                'value' => function ($data) {
                    return '<img src="' . $data->user->profile->photo . '">';
                }
            ],
            [
                'attribute' => 'user_id',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::a($data->user->profile->username,
                        Url::to(['/admin/user/view', 'id' => $data->user->id])
                    );

                },
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'profile.username')

            ],

            [
                'attribute' => 'created_at',
                'format' => 'html',
                'value' => function ($data) {
                    return date('d.m.Y', $data->created_at) . ' <small>' .
                    date('H:i:s', $data->created_at) . '</small>';
                }
            ],
            [
                'attribute' => 'status',
                'value' => function ($data) {
                    return Sms::getStatusesArray()[$data->status];
                }
            ],
        ],
    ]); ?>

</div>
