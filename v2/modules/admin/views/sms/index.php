<?php

use common\models\Sms;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'SMS';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sending-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'created_at',
                'format' => 'html',
                'value' => function ($data) {
                    return date('d.m.Y', $data->created_at) . ' <small>' .
                    date('H:i:s', $data->created_at) . '</small>';
                }
            ],
            [
                'attribute' => 'status',
                'value' => function ($data) {
                    return Sms::getStatusesArray()[$data->status];
                }
            ],

            'message:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]); ?>

</div>
