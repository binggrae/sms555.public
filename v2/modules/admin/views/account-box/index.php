<?php

use common\widgets\Alert;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Активные коробки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-box-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo Alert::widget(); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'account_id',
                'value' => function ($model, $key, $index, $column) {
                    return $model->account->login . ' (' . $model->{$column->attribute} . ')';
                }
            ],
            [
                'attribute' => 'box_id',
                'value' => function ($model) {
                    return Html::a($model->box->name, $model->box->link);
                },
                'format' => 'raw',
                'filter' => function() {

                }
            ],
            [
                'attribute' => 'date',
                'format' => ['date', 'HH:mm:ss dd.MM.Y'],
            ],
            [
                'attribute' => 'is_order',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    $value = $model->{$column->attribute};

                    if ($value == 1 ) {
                        $text = 'Да';
                        $class = 'success';
                    } else {
                        $text = 'Нет';
                        $class = 'warning';
                    }
                    $html = Html::tag('span', $text, ['class' => 'label label-' . $class]);
                    return $value === null ? $column->grid->emptyCell : $html;
                }

            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}'
            ],
        ],
    ]); ?>

</div>
