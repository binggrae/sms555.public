<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AccountBox */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Активные коробки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-box-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'account_id',
                'format' => 'raw',
                'value' => $model->account->login . ' ' .
                    Html::a(
                        '#' . $model->account_id,
                        Url::to(['/admin/account/proxy', 'id' => $model->account_id]),
                        ['target' => '_blank']

                    )
            ],
            [
                'attribute' => 'box_id',
                'value' => Html::a($model->box->name, $model->box->link),
                'format' => 'raw'
            ],
            [
                'attribute' => 'date',
                'format' => ['date', 'HH:mm:ss dd.MM.Y'],
            ],
            [
                'attribute' => 'is_order',
                'format' => 'raw',
                'value' => Html::tag('span', $model->is_order == 1 ? 'Да' : 'Нет',
                    ['class' => 'label label-' . ($model->is_order == 1 ? 'success' : 'warning')]),

            ]
        ],
    ]) ?>

    <div class="row">
        <div class="col-md-6">
            <h5>Текст для первого письма</h5>
            <blockquote>
                <p>Реквизиты карты СБ: 4276 8720 9141 9201 Держатель карты, Надежда Андреевна П.</p>

                <p>Яндекс кошелёк: 41001894137683</p>

                <p>Qiwi: 8 (982) 290-05-44</p>

                <p>В комментарии к платежу укажите фамилию того, на кого оформлялась коробочка</p>
            </blockquote>
            <h5>Текст для второго письма</h5>
            <blockquote>
                <p>Реквизиты для сайты Крыгиной</p>

                <p>Логин: <?= $model->account->login; ?></p>

                <p>Пароль: <?= $model->account->password; ?></p><br/>

                <p>Реквизиты для входа в почту</p>

                <p>Логин: <?= $model->account->login; ?></p>

                <p>Пароль: <?= $model->account->mail_password; ?></p><br/>

                <p>Рекомендуем изменить пароли от почты и сайта Крыгиной</p>

                <p>Внимание, в корзине коробочки нет, она находится в личном кабинете! Для того, чтобы ее найти,
                    посмотрите скриншот, который прикреплен к этому письму. Следуйте указаниям и у вас все получится</p>
            </blockquote>
        </div>
    </div>


</div>
