<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Капчи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="captcha-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'key',
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($data) {
                    return '<img src="' . Url::to(['/admin/captcha/view', 'id' => $data->id]) . '">';
                }
            ],
            [
                'attribute' => 'date',
                'format' => 'html',
                'value' => function ($data) {
                    return date('d.m.Y', $data->date) . ' <small>' .
                    date('H:i:s', $data->date) . '</small>';
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}'
            ],
        ],
    ]); ?>

</div>
