<?php

use common\models\Sms;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
    <br/>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'columns' => [
        [
            'attribute' => 'created_at',
            'format' => 'html',
            'value' => function ($data) {
                return date('d.m.Y', $data->created_at) . ' <small>' .
                date('H:i:s', $data->created_at) . '</small>';
            }
        ],
        [
            'attribute' => 'Текст',
            'value' => function ($data) {
                return $data->sending->message;
            }
        ],
        [
            'attribute' => 'status',
            'value' => function ($data) {
                return Sms::getStatusesArray()[$data->status];
            }
        ],
    ],
]); ?>