<?php

use common\models\Transaction;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $transaction common\models\Transaction */
?>
    <br/>
    <div class="activate-form">
        <?php $form = ActiveForm::begin([
            'action' => Url::to('/transaction/default/manual')
        ]); ?>


        <?= $form->field($transaction, 'period')->textInput(); ?>

        <?= $form->field($transaction, 'user_id', ['template' => '{input}'])->hiddenInput(['value' => $model->id]) ?>

        <div class="form-group">
            <?= Html::submitButton('Активировать', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'count',
        'period',
        [
            'attribute' => 'service',
            'value' => function ($data) {
                return Transaction::getServiceArray()[$data->service];
            }
        ],
        [
            'attribute' => 'created_at',
            'format' => 'html',
            'value' => function ($data) {
                return date('d.m.Y', $data->created_at) . ' <small>' .
                date('H:i:s', $data->created_at) . '</small>';
            }
        ],
    ],
]); ?>