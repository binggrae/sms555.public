<?php

use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Пользователи | sms555.ru';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <hr/>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'profile.photo',
                'header' => '',
                'format' => 'html',
                'value' => function ($data) {
                    return '<img src="' . $data->profile->photo . '">';
                }
            ],
            [
                'attribute' => 'profile.username',
                'format' => 'html',
                'value' => function ($data) {
                    return '<a href="https://vk.com/id' . $data->id . '">' . $data->profile->username . '</a>';
                }

            ],
            [
                'attribute' => 'phone',
                'format' => 'html',
                'filter' => false
            ],
            [
                'attribute' => 'created_at',
                'format' => 'html',
                'value' => function ($data) {
                    return date('d.m.Y', $data->created_at) . ' <small>' .
                    date('H:i:s', $data->created_at) . '</small>';
                },
                'filter' => false
            ],
            [
                'attribute' => 'status',
                'value' => function($data) {
                    return User::getStatusesArray()[$data->status];
                },
                'filter' => User::getStatusesArray()
            ],
            [
                'attribute' => 'last_login',
                'format' => 'html',
                'value' => function ($data) {
                    return date('d.m.Y', $data->last_login) . ' <small>' .
                    date('H:i:s', $data->last_login) . '</small>';
                },
                'filter' => false
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]); ?>

</div>