<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $transaction common\models\Transaction */
/* @var $transactionProvider yii\data\ActiveDataProvider */
/* @var $smsProvider yii\data\ActiveDataProvider */


$this->title = $model->profile->username .' | sms555.ru';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'profile.photo',
                'format' => 'html',
                'value' => '<img src="' . $model->profile->photo . '">'
            ],
            [
                'attribute' => 'profile.username',
                'format' => 'html',
                'value' => '<a href="https://vk.com/id' . $model->id . '">' . $model->profile->username . '</a>',
            ],
            'phone',
            [
                'attribute' => 'profile.gender',
                'value' => $model->profile->gender == 2 ? 'Мужской' : 'Женский',
            ],
            'profile.city_str',
            'profile.country_str',
            [
                'attribute' => 'created_at',
                'format' => 'html',
                'value' => date('d.m.Y', $model->created_at) . ' <small>' .
                    date('H:i:s', $model->created_at) . '</small>'
            ],
            [
                'attribute' => 'active_to',
                'format' => 'html',
                'value' =>  date('d.m.Y', $model->active_to) . ' <small>' .
                    date('H:i:s', $model->active_to) . '</small>'
            ],
            [
                'attribute' => 'role',
                'value' => $model->getRoleName(),
            ],
            [
                'attribute' => 'status',
                'value' => $model->getStatusName(),
            ],
        ],
    ]) ?>

    <hr/>

    <?php echo Tabs::widget([
        'items' => [
            [
                'label' => 'Транзакции',
                'content' => $this->render('_transaction', [
                    'model' => $model,
                    'dataProvider' => $transactionProvider,
                    'transaction' => $transaction
                ])
            ],
            [
                'label' => 'SMS',
                'content' => $this->render('_sms', ['dataProvider' => $smsProvider]),
            ],
        ]
    ]); ?>


</div>
