<?php

use common\models\Order;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = $model->ORDER_PROP_3;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Так же удалиться и связанный аккаунт. Коробка сброшена не будет',
                'method' => 'post',
            ],
        ]) ?>

        <?php $form = ActiveForm::begin([
            'action' => ['/admin/order/status']
        ]); ?>

        <?= $form->field($model, 'status')->dropDownList(Order::getStatusesLabel()) ?>
        <?= Html::activeHiddenInput($model, 'id'); ?>
        <?= Html::submitButton('Изменить', ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end(); ?>
    </p>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'account_id',
                        'format' => 'raw',
                        'value' => $model->account->login . ' ' .
                            Html::a(
                                '#' . $model->account_id,
                                Url::to(['/admin/account/proxy', 'id' => $model->account_id]),
                                ['target' => '_blank']

                            )
                    ],
                    [
                        'attribute' => 'box_id',
                        'value' => Html::a($model->box->name, $model->box->link),
                        'format' => 'raw'
                    ],
                    'ORDER_PROP_3',
                    'email',
                    'ORDER_PROP_7',
                    [
                        'attribute' => 'type',
                        'format' => 'raw',
                        'value' => $model->getTypeLabel()
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => $model->getStatusLabel()
                    ],
                    [
                        'attribute' => 'service',
                        'format' => 'raw',
                        'value' => $model->getServiceLabel()
                    ],
                    'comment'
                ],
            ]) ?>
            <hr/>
            <?php if ($model->oa_id) : ?>
                <?= DetailView::widget([
                    'model' => $model->oa,
                    'attributes' => [
                        'login',
                        'password'
                    ],
                ]) ?>
            <?php endif; ?>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'ORDER_PROP_6_val',
                    'ORDER_PROP_6',
                    'ORDER_PROP_5',
                    'ORDER_PROP_10',
                    'ORDER_PROP_11',
                    'ORDER_PROP_12',
                    'ORDER_PROP_13',
                    'TRACKING_NUMBER',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <h5>Текст для первого письма</h5>
            <blockquote>
                <p>Реквизиты карты СБ: 4276 8720 9141 9201 Держатель карты, Надежда Андреевна П.</p>

                <p>Яндекс кошелёк: 41001894137683</p>

                <p>Qiwi: 8 (982) 290-05-44</p>

                <p>В комментарии к платежу укажите фамилию того, на кого оформлялась коробочка</p>
            </blockquote>
            <h5>Текст для второго письма</h5>
            <blockquote>
                <p>Реквизиты для сайты Крыгиной</p>

                <p>Логин: <?= $model->account->login; ?></p>

                <p>Пароль: <?= $model->account->password; ?></p><br/>

                <p>Реквизиты для входа в почту</p>

                <p>Логин: <?= $model->account->login; ?></p>

                <p>Пароль: <?= $model->account->mail_password; ?></p><br/>

                <p>Рекомендуем изменить пароли от почты и сайта Крыгиной</p>

                <p>Внимание, в корзине коробочки нет, она находится в личном кабинете! Для того, чтобы ее найти,
                    посмотрите скриншот, который прикреплен к этому письму. Следуйте указаниям и у вас все получится</p>
            </blockquote>

        </div>
    </div>

</div>
