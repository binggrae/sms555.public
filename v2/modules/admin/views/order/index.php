<?php

use common\models\AccountBox;
use common\models\Box;
use common\models\Order;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\models\Order */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Самовывоз', ['create', 'type' => 'manual'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Пикпоинт', ['create', 'type' => 'point'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Курьер', ['create', 'type' => 'delivery'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',

            [
                'attribute' => 'box_id',
                'value' => function ($model, $key, $index, $column) {
                    return Html::a($model->box->name, $model->box->link);
                },
                'format' => 'raw',
                'filter' => Box::getList(),
            ],
            [
                'attribute' => 'account_id',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    return $model->account ?
                        $model->account->login . ' (' . $model->{$column->attribute} . ')' :
                        Html::tag('span', 'Нет', ['class' => 'label label-warning']);
                },
                'filter' => [1=> 'Не оформлен', 2=>'Оформлен'],
            ],
            'ORDER_PROP_3',
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var Order $model */
                    return $model->getTypeLabel();
                },
                'filter' => Order::getTypesLabel()
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var Order $model */
                    return $model->getStatusLabel();
                },
                'filter' => Order::getStatusesLabel()
            ],

            [
                'attribute' => 'service',
                'format' => 'raw',
                'value' => function ($model) {
                    /** @var Order $model */
                    return $model->getServiceLabel();
                },
                'filter' => Order::getServicesLabel()
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {
                        /** @var Order $model */
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>',
                            Url::to(['update', 'type' => $model->getTypeLink(), 'id' => $model->id]));
                    },
                ],
            ],
        ],
    ]); ?>

</div>
