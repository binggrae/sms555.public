<?php

use common\models\Box;
use common\models\Order;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $account common\models\Account */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">


    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <label class="control-label"><span>Поиск</span>
            <?= AutoComplete::widget([
                'options' => [
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'source' => Url::to(['/admin/order/history']),
                    'select' => new JsExpression("function(e, ui) {
                        var model = '" . $model::MODEL_NAME . "';
                        for(var i in ui.item.data) {
                            $('#' + model + '-' + i.toLowerCase() ).val(ui.item.data[i]);
                        }
                        $('#pointorder-tracking_number').remove();
                        $('.field-pointorder-tracking_number').append(
                            '<input type=\"text\" id=\"pointorder-tracking_number\"'
                            + ' class=\"form-control\" name=\"PointOrder[TRACKING_NUMBER]\"'
                            + ' value=\" '+ui.item.data[\"TRACKING_NUMBER\"]+'\">'
                        )
                    }")
                ],
            ]); ?>
        </label>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'box_id')->dropDownList(Box::getList()) ?>

            <?= $form->field($model, 'ORDER_PROP_3')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ORDER_PROP_7')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">

            <?= $form->field($account, 'login')->textInput() ?>

            <?= $form->field($account, 'password')->textInput() ?>

            <?= $form->field($model, 'service')->dropDownList(Order::getServicesLabel()) ?>

            <?= $form->field($model, 'comment')->textInput() ?>

        </div>
    </div>
    <hr/>

    <?= $form->field($model, 'ORDER_PROP_6_val')->widget(\yii\jui\AutoComplete::classname(), [
        'options' => [
            'class' => 'form-control'
        ],
        'clientOptions' => [
            'source' => Url::to(['/admin/order/search']),
            'select' => new JsExpression("function(e, ui) {
                $('#pointorder-order_prop_6').val(ui.item.id);
                $.ajax({
                    url: '" . Url::to(['/admin/pickpoint/search']) . "',
                    data: {city: ui.item.city},
                    success: function(data) {
                        window.point = {};
                        $('#pointorder-uid option').remove();
                        for(var i in data) {
                            window.point[data[i].id] = data[i];
                            var el = $('<option value=\"'+ data[i].id +'\">'+data[i].label+'</option>');
                            el.on('click', function() {
                                var prop = window.point[$(this).val()].prop;

                                $('#pointorder-order_prop_5').val(prop['ZIP_CODE']);
                                $('#pointorder-order_prop_10').val(prop.STREET);
                                $('#pointorder-order_prop_11').val(prop.HOUSE);
                                $('#pointorder-tracking_number').val('#' + prop.ID + ', ' + prop.NAME);
                            });
                            $('#pointorder-uid').append(el);
                        }
                    },
                    error: function() {
                        console.log(arguments);
                    },
                    dataType: 'JSON'
                });
            }")
        ],
    ]) ?>

    <?= $form->field($model, 'uid')->dropDownList([], ['multiple' => '']) ?>




    <?= $form->field($model, 'ORDER_PROP_5')->textInput() ?>
    <?= $form->field($model, 'ORDER_PROP_10')->textInput() ?>
    <?= $form->field($model, 'ORDER_PROP_11')->textInput() ?>
    <?= $form->field($model, 'TRACKING_NUMBER')->textInput() ?>

    <?= $form->field($model, 'ORDER_PROP_6')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
