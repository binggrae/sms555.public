<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $account common\models\Account */
/* @var $model common\models\Order */

$this->title = 'Пикпоинт';
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'account' => $account,
    ]) ?>

</div>
