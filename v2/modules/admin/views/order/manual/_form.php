<?php

use common\models\Box;
use common\models\Order;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $account common\models\Account */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">


    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <label class="control-label"><span>Поиск</span>
            <?= AutoComplete::widget([
                'options' => [
                    'class' => 'form-control'
                ],
                'clientOptions' => [
                    'source' => Url::to(['/admin/order/history']),
                    'select' => new JsExpression("function(e, ui) {
                        var model = '" . $model::MODEL_NAME . "';
                        for(var i in ui.item.data) {
                            $('#' + model + '-' + i.toLowerCase() ).val(ui.item.data[i]);
                        }
                    }")
                ],
            ]); ?>
        </label>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'box_id')->dropDownList(Box::getList()) ?>

            <?= $form->field($model, 'ORDER_PROP_3')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'ORDER_PROP_7')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($account, 'login')->textInput() ?>

            <?= $form->field($account, 'password')->textInput() ?>

            <?= $form->field($model, 'service')->dropDownList(Order::getServicesLabel()) ?>

            <?= $form->field($model, 'comment')->textInput() ?>

        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
