<?php

use miloschuman\highcharts\Highcharts;
use yii\helpers\VarDumper;

/** @var Array &data */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\models\Order */

$this->title = 'Административная панель';


//VarDumper::dump($data['sms']['user'], 10, 1);
?>

<div class="row">
    <div class="col-md-12">
        <h1>Статистика</h1>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        http://188.225.33.235:9001/
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php
        echo Highcharts::widget([
            'options' => [
                'chart' => [
                    'type' => 'column'
                ],
                'plotOptions' => [
                    'column' => [
                        'stacking' => 'normal',
                        'depth' => 40,
                    ]
                ],

                'title' => ['text' => 'Статистика аккаунтов'],
                'xAxis' => [
                    'categories' => $data['boxes']['item']
                ],
                'yAxis' => [
                    'title' => ['text' => '']
                ],
                'series' => [
                    ['name' => 'Всего', 'stack' => 'count', 'data' => $data['boxes']['all']],
                    ['name' => 'Активных', 'stack' => 'count', 'data' => $data['boxes']['active']]
                ]
            ]
        ]);
        ?>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-md-6">
        <div class="head">
            <h2>SMS</h2>
        </div>
        <?php
        echo Highcharts::widget([
            'options' => [
                'chart' => [
                    'type' => 'column'
                ],
                'title' => ['text' => 'Ститистика по месяцам'],
                'xAxis' => [
                    'categories' => $data['sms']['item']
                ],
                'yAxis' => [
                    'title' => ['text' => '']
                ],
                'series' => [
                    ['name' => 'Всего', 'data' => $data['sms']['value']],
                ]
            ]
        ]);
        ?>

        <?php
        echo Highcharts::widget([
            'options' => [
                'chart' => [
                    'type' => 'pie'
                ],
                'title' => ['text' => 'Ститистика по пользователям'],
                'plotOptions' => [
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' => 'cursor',
                        'dataLabels' => [
                            'enabled' => true,
                            'format' => '<b>{point.name}</b>: {point.percentage:.1f} %',

                        ]

                    ]
                ],

                'series' => [
                    [
                        'name' => 'Всего',
                        'colorByPoint' => true,
                        'data' => $data['sms']['user'],
                    ]
                ]
            ]
        ]);
        ?>

    </div>
</div>