<?php

use common\models\Transaction;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Оплаты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'user.profile.photo',
                'header' => '',
                'format' => 'html',
                'value' => function ($data) {
                    return '<img src="' . $data->user->profile->photo . '">';
                }
            ],
            [
                'attribute' => 'user_id',
                'format' => 'html',
                'value' => function ($data) {
                    return '<a href="https://vk.com/id' . $data->user->id . '">' . $data->user->profile->username . '</a>';
                },
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'profile.username')

            ],


            [
                'attribute' => 'service',
                'value' => function ($data) {
                    return Transaction::getServiceArray()[$data->service];
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => 'html',
                'value' => function ($data) {
                    return date('d.m.Y', $data->created_at) . ' <small>' .
                    date('H:i:s', $data->created_at) . '</small>';
                }
            ],
            'period',
            'count',
        ],
    ]); ?>

</div>
