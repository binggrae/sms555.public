<?php

namespace v2\modules\admin;

class Admin extends \yii\base\Module
{
    public $controllerNamespace = 'v2\modules\admin\controllers';

    public function init()
    {
        parent::init();

        $this->layoutPath = \Yii::getAlias('@v2/modules/admin/views/layouts');
        $this->layout = 'main';
    }
}
