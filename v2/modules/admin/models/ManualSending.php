<?php

namespace v2\modules\admin\models;

use common\models\Sending;
use yii\helpers\ArrayHelper;

class ManualSending extends Sending
{

    public $from;

    const SEND_ALL = 0;
    const SEND_ACTIVE = 1;
    const SEND_INACTIVE = 2;


    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
                [['from'], 'required'],
                [['from'], 'integer']
            ]
        );
    }


    public function create_send()
    {
        var_dump($this->from);
        switch ($this->from) {
            case self::SEND_ALL:
                parent::sendAll($this->message);
                break;
            case self::SEND_ACTIVE:
                parent::sendByActive($this->message);
                break;
            case self::SEND_INACTIVE:
                parent::sendByInactive($this->message);
                break;
        }
    }

    public static function getFromLabel()
    {
        return [
            self::SEND_ACTIVE => 'Отправить активным',
            self::SEND_INACTIVE => 'Отправить не активным',
            self::SEND_ALL => 'Отправить всем',
        ];
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(), [
                'from' => 'Кому'
            ]
        );
    }

}
