<?php

namespace v2\modules\admin\models;

use Yii;

/**
 * This is the model class for table "account".
 * @property array $status
 */
class Account extends \common\models\Account
{

    public function getStatus()
    {
        $data = ['all' => rand(0, 5)];
        $data['current'] = rand(0, $data['all']);
        return $data;
    }

    public function getCount()
    {
        $r = rand(0,5);
        return $r == 0 ? ['1200' => date('d.m.Y H:i:s', time() + 7444)] : null;
    }
}
