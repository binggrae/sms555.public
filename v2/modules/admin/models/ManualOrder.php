<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 04.11.2015
 * Time: 20:04
 */

namespace v2\modules\admin\models;


use common\models\Order;
use common\models\Pikpoint;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class ManualOrder extends Order
{
    const DELIVERY_ID = 1;

    const MODEL_NAME = 'manualorder';

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [[
            [
                'box_id',
                'ORDER_PROP_3',
                'ORDER_PROP_7',
            ], 'required'],
        ]);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->type = self::TYPE_MANUAL;
            $this->DELIVERY_ID = self::DELIVERY_ID;

            $this->ORDER_PROP_6_val = self::DEFAULT_CITY;
            $this->ORDER_PROP_6 = self::DEFAULT_CITY_ID;
            $this->ORDER_PROP_5 = self::DEFAULT_CODE;

        }
        return parent::beforeSave($insert);
    }

}