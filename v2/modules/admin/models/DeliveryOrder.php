<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 04.11.2015
 * Time: 20:04
 */

namespace v2\modules\admin\models;


use common\models\Order;
use common\models\Pikpoint;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class DeliveryOrder extends Order
{
    const DELIVERY_ID = 6;

    const MODEL_NAME = 'deliveryorder';

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [[
            [
                'box_id',
                'ORDER_PROP_3',
                'ORDER_PROP_7',
                'ORDER_PROP_6_val',
                'ORDER_PROP_6',
                'ORDER_PROP_5',
                'ORDER_PROP_10',
                'ORDER_PROP_11',
            ], 'required'],
        ]);
    }

    public function beforeSave($insert)
    {
        if($insert) {
            $this->type = self::TYPE_DELIVERY;
            $this->DELIVERY_ID = self::DELIVERY_ID;

        }
        return parent::beforeSave($insert);
    }

}