<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 05.11.2015
 * Time: 15:50
 */

namespace v2\modules\admin\controllers;


use common\components\Curl;
use common\components\Watcher;
use common\models\Pikpoint;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;

class PickpointController extends Controller
{

    public function actionSearch($city)
    {
        $city = mb_convert_case($city, MB_CASE_LOWER, "UTF-8");


        if ($city != 'москва') {
            $params = http_build_query([
                'city' => mb_convert_case($city, MB_CASE_LOWER, "UTF-8")
            ]);
            $link = 'http://elenakrygina.com/bitrix/templates/.default/' .
                'components/idesigning/sale.order.ajax/ircit_sale_order/postamats.php?' . $params;

            $curlRequest = new Curl();

            $curlRequest->link = $link;
            $curlRequest->chunked = true;
            $curlRequest->headers = [
                'Accept-Encoding: deflate, sdch',
                'User-Agent: Mozilla/5.0',
                'Accept: text/plain',
                'Cache-Control: no-cache',
                'Connection:keep-alive',
                'Host:elenakrygina.com',
                'Upgrade-Insecure-Requests:1',
            ];

            $result = $curlRequest->GetRequest();

            $data = json_decode($result)->GLOBAL;
        } else {
            /** @var Pikpoint $model */
            $model = Pikpoint::find()->where(['city' => $city])->orderBy('address ASC')->all();

            $data = [];
            foreach ($model as $item) {
                $class = new \stdClass();

                foreach ($item as $attr => $value) {
                    $attr = mb_convert_case($attr, MB_CASE_UPPER, "UTF-8");
                    $class->$attr = $value;
                }

                $data[] = $class;
            }
        }


        $result = [];
        foreach ($data as $item) {
            $result[] = [
                'id' => $item->ID,
                'label' => $item->ADDRESS,
                'prop' => $item
            ];
        }

        echo json_encode($result);
    }


    public function __actionSearch($city)
    {
        /** @var Pikpoint[] $model */
        $model = Pikpoint::find()->where(['city' => $city])->orderBy('address ASC')->all();

        $result = [];
        foreach ($model as $item) {
            $result[] = [
                'id' => $item->id,
                'label' => $item->address,
            ];
        }

        echo json_encode($result);
    }

}