<?php

namespace v2\modules\admin\controllers;

use common\components\Watcher;
use common\models\Account;
use common\models\AccountBox;
use common\models\OuterAccount;
use v2\modules\admin\models\DeliveryOrder;
use v2\modules\admin\models\ManualOrder;
use v2\modules\admin\models\PointOrder;
use Yii;
use common\models\Order;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $model = new Order();
        $dataProvider = $model->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionCreate($type)
    {
        if ($type == 'point') {
            $model = new PointOrder();
        } elseif ($type == 'manual') {
            $model = new ManualOrder();
        } elseif ($type == 'delivery') {
            $model = new DeliveryOrder();
        } else {
            throw new HttpException(404, 'Неверный тип заказа');
        }


        $account = new Account(['scenario' => Account::SCENARIO_OTHER_ACCOUNT]);

        if ($model->load(Yii::$app->request->post())) {

            $data = Yii::$app->request->post('Account');
            $account->login = $data['login'];
            $account->password = $data['password'];

            if ($account->login) {
                $model->email = $account->login;
                $account->mail_password = '';
                $account->phone = $model->ORDER_PROP_7;
                $account->save(0);
                $model->oa_id = $account->id;
            }

            if(!$model->save()) {
                VarDumper::dump($model->getErrors());
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render($type . '/create', [
                'model' => $model,
                'account' => $account,
            ]);
        }
    }


    public function actionUpdate($id, $type)
    {
        if ($type == 'point') {
            $model = new PointOrder();
        } elseif ($type == 'manual') {
            $model = new ManualOrder();
        } elseif ($type == 'delivery') {
            $model = new DeliveryOrder();
        } else {
            throw new HttpException(404, 'Неверный тип заказа');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render($type . '/update', [
                'model' => $model,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = Order::STATUS_DELETE;
        $model->save(0);

        return $this->redirect(['index']);
    }

    public function actionStatus()
    {
        $id = Yii::$app->request->post('Order')['id'];
        $model = $this->findModel($id);
        $model->load(Yii::$app->request->post());

        if($model->status == Order::STATUS_CLOSE) {
            $model->account->is_delete=1;
            $model->account->save(0);
        }

        $model->save(0);

        return $this->redirect(['/admin/order/view', 'id' => $id]);
    }


    public function actionSearch($term)
    {
        $str = str_replace('"', '', str_replace('\\', '%', json_encode($term)));
        $ch = curl_init('http://elenakrygina.com/bitrix/components/bitrix/sale.ajax.locations/search.php?search=' . $str);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => 1,
        ]);
        $data = iconv('cp1251', 'utf-8', curl_exec($ch));

        $data = json_decode(str_replace("'", '"', $data));

        $result = [];
        foreach ($data as $item) {
            if (!$item->NAME) {
                continue;
            }
            $result[] = [
                'id' => $item->ID,
                'city' => $item->NAME,
                'value' => $item->NAME . ', ' . $item->REGION_NAME . ', ' . $item->COUNTRY_NAME,
                'label' => $item->NAME . ', ' . $item->REGION_NAME . ', ' . $item->COUNTRY_NAME
            ];
        }

        echo json_encode($result);
    }

    public function actionHistory($term)
    {
        $phone = Order::editPhone($term);
        /** @var Order[] $orders */
        $orders = Order::find()
            ->where(['LIKE', 'ORDER_PROP_7', $phone])
            ->groupBy('ORDER_PROP_7')
            ->all();

        $result = [];
        foreach ( $orders as $order) {
            $result[] = [
                'id' => $order->id,
                'label' => $order->ORDER_PROP_3,
                'data' => $order->getAttributes([
                    'email',
                    'ORDER_PROP_3',
                    'ORDER_PROP_7',
                    'ORDER_PROP_6_val',
                    'ORDER_PROP_6',
                    'ORDER_PROP_5',
                    'ORDER_PROP_10',
                    'ORDER_PROP_11',
                    'ORDER_PROP_12',
                    'ORDER_PROP_13',
                    'TRACKING_NUMBER',
                    'DELIVERY_ID',
                ])
            ];
        }

        echo json_encode($result);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
