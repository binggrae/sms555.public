<?php

namespace v2\modules\admin\controllers;

use common\models\Sms;
use common\models\Transaction;
use Yii;
use common\models\User;
use common\models\search\UserSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $transactionProvider = new ActiveDataProvider([
            'query' => Transaction::find()
                ->where(['user_id' => $model->id])
                ->orderBy('id DESC'),
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);
        $smsProvider = new ActiveDataProvider([
            'query' => Sms::find()
                ->where(['user_id' => $model->id])
                ->with('sending')
                ->orderBy('id DESC'),
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);


        $transaction = new Transaction();

        return $this->render('view', [
            'model' => $model,
            'transactionProvider' => $transactionProvider,
            'smsProvider' => $smsProvider,
            'transaction' => $transaction
        ]);
    }


    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
