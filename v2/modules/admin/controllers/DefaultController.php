<?php

namespace v2\modules\admin\controllers;

use common\models\Account;
use common\models\AccountBox;
use common\models\Box;
use common\models\Profile;
use common\models\Sending;
use common\models\Sms;
use common\models\Transaction;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $data = [];

        // ===================== STATISTIC BOX
        /** @var Box[] $boxes */
        $boxes = Box::find()->All();

        foreach ($boxes as $box) {
            $data['boxes']['item'][] = $box->name;
            $data['boxes']['all'][] = 0 + Account::find()->where(['box_id' => $box->uid])->count();
            $data['boxes']['active'][] = 0 + AccountBox::find()->where(['box_id' => $box->uid])->count();
        }

        /** @var Transaction[] $transactions */
        $transactions = Transaction::find()->all();
        foreach ($transactions as $tr) {
            $data['transaction']['item'][] = $tr->period;

        }

        // ===================== STATISTIC SMS BY MOUNT
        /** @var Sending[] $sending */
        $sending = Sending::find()->all();
        $sms_data = [];
        foreach ($sending as $sms) {
            if (!isset($sms_data['item'][date('d-m-y', $sms->created_at)])) {
                $sms_data['item'][date('d-m-y', $sms->created_at)] = date('d-m-y', $sms->created_at);
            }
            $sms_data['value'][date('d-m-y', $sms->created_at)]++;
        }

        foreach ($sms_data['item'] as $item) {
            $data['sms']['item'][] = $item;
        }
        foreach ($sms_data['value'] as $item) {
            $data['sms']['value'][] = $item;
        }

        // ===================== STATISTIC SMS BY USER
        /** @var Sms[] $sms */
//        'select count(id) from sms group by user_id having user_id;';
        $sms = ( new Query() )
            ->select('user_id, count(id) as count')
            ->from('sms')
            ->groupBy('sms.user_id')
            ->having('sms.user_id')
            ->all();

        foreach ($sms as $item) {
            /** @var Profile $profile */
            $profile = Profile::find()
                ->select('username')
                ->where(['user_id' => $item['user_id']])->one();
            $data['sms']['user'][] = [
                'name' => $profile->username,
                'y' => 0 + $item['count']
            ];
        }

        return $this->render('index', [
            'data' => $data
        ]);
    }


}
