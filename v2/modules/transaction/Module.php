<?php

namespace v2\modules\transaction;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'v2\modules\transaction\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
