<?php

namespace v2\modules\transaction\controllers;

use common\models\Transaction;
use v2\models\TransactionForm;
use v2\modules\transaction\models\YandexTransaction;
use yii\web\Controller;

class DefaultController extends Controller
{

    public $enableCsrfValidation = false;

    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionRun()
    {
        $model = new TransactionForm();
        if ($model->load(\Yii::$app->request->post())) {
            $this->redirect('https://money.yandex.ru/quickpay/confirm.xml?' . http_build_query($model->getData()));
        }
    }

    public function actionManual()
    {
        $model = new Transaction();
        $model->load(\Yii::$app->request->post());
        $model->created_at = time();
        $model->count = 0;
        $model->service = Transaction::SERVICE_MANUAL;

        $model->save();

        return $this->redirect(['/admin/user/view', 'id' => $model->user_id]);

    }

    public function actionYandex()
    {
        $model = new YandexTransaction($_POST);
        if ($model->validate()) {
            $model->save();
        } else {
            \Yii::error($model->getErrors(), 'yandex');
        }
    }

    public function actionTest()
    {
        $data = [
            'notification_type' => 'card-incoming',
            'label' => 'SMS555.30.195521942',
            'amount' => '98.00',
            'datetime' => '2015-05-19T19:25:26Z',
            'codepro' => 'false',
            'withdraw_amount' => '100.00',
            'sender' => '',
            'sha1_hash' => '878118aa2cee2c6a2ff75fe11454d7d674bf6f2c',
            'unaccepted' => 'false',
            'operation_label' => '1ceda1b5-0002-5000-8032-e089cc7f67cd',
            'operation_id' => '485378726830055012',
            'currency' => '643',
        ];

        $model = new YandexTransaction($_POST);
        if ($model->validate()) {
            $model->save();
        } else {
            \Yii::error($model->getErrors(), 'yandex');
        }
    }


}
