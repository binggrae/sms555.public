<?php

namespace v2\modules\transaction\models;

use common\models\Transaction;
use common\models\User;
use yii\base\Model;
use yii\helpers\VarDumper;

class YandexTransaction extends Model
{

    public $notification_type;
    public $operation_id;
    public $test_notification;
    public $amount;
    public $currency;
    public $datetime;
    public $sender;
    public $codepro;
    public $label;
    public $unaccepted;
    public $withdraw_amount;
    public $operation_label;
    public $service;
    public $period;
    public $sha1_hash;

    protected $secret = 'eGXX8luzwgSVXnq9FtnwOQDZ';

    protected $user_id;


    public function rules()
    {
        return [
            [[
                'notification_type',
                'operation_id',
                'amount',
                'currency',
                'datetime',
                'codepro',
                'withdraw_amount',
                'sha1_hash',
            ], 'required'],

            [['sha1_hash'], 'checkHash'],
            [['user_id'], 'checkUser'],
            [['codepro'], 'checkCodePro'],
            [['withdraw_amount'], 'checkAmount'],
            [['unaccepted'], 'checkUnaccepted']
        ];
    }


    public function beforeValidate()
    {
        $label = $this->label;
        preg_match('/^SMS555\.(\d+)\.(\d+)$/', $label, $math);
        if (count($math) < 2) {
            $this->addError('label', 'Не верный формат label');
            return false;
        }
        $this->secret = \Yii::$app->params['yandex_secret'];

        $this->period = $math[1];
        $this->user_id = $math[2];


        $this->service = $this->notification_type == 'p2p-incoming' ?
            Transaction::SERVICE_YM :
            Transaction::SERVICE_CARD;

        VarDumper::dump($this->attributes, 10, 1);
        VarDumper::dump((100 == $this->withdraw_amount && $this->period != 30), 10, 1);

        return parent::beforeValidate();
    }

    // VALIDATORS
    public function checkHash($attribute)
    {
        $string =
            $this->notification_type . '&' .
            $this->operation_id . '&' .
            $this->amount . '&' .
            $this->currency . '&' .
            $this->datetime . '&' .
            $this->sender . '&' .
            $this->codepro . '&' .
            $this->secret. '&' .
            $this->label;

        if ($this->$attribute != sha1($string)) {
            $this->addError($attribute, 'Неверный хеш');
        }
    }

    public function checkUser($attribute)
    {
        $user = User::findOne(['id' => $this->$attribute]);
        if (!$user) {
            $this->addError($attribute, 'Пользователь не найден');
        }
    }

    public function checkCodePro($attribute)
    {
        if ($this->$attribute === 'true') {
            $this->addError($attribute, 'Запрещены платежы с кодом протекции');
        }
    }

    public function checkAmount($attribute)
    {
        if (
            Transaction::PRICE_30 != $this->$attribute &&
            Transaction::PRICE_90 != $this->$attribute
        ) {
            $this->addError($attribute, 'Неверная сумма платежа. Неккоректная сумма');
        }

        if (Transaction::PRICE_30 == $this->$attribute && $this->period != 30) {
            $this->addError($attribute, 'Неверная сумма платежа. Сумма не совпадает с выбранным периодом');
        } elseif (Transaction::PRICE_90 == $this->$attribute && $this->period != 90) {
            $this->addError($attribute, 'Неверная сумма платежа. Сумма не совпадает с выбранным периодом');
        }
    }


    public function checkUnaccepted($attribute)
    {
        var_dump($this->$attribute);
        if ($this->$attribute == 'true') {
            $this->addError($attribute, 'Невозможно получить платеж');
        }
    }

    // GETTERS
    public function getUser_id()
    {
        return $this->user_id;
    }

    public function getPeriod()
    {
        return $this->period;
    }

    public function save()
    {
        $model = new Transaction([
			'created_at' => time(),
            'user_id' => $this->user_id,
            'count' => $this->withdraw_amount,
            'service' => $this->service,
            'period' => $this->period,
        ]);
        return $model->save();
    }


}