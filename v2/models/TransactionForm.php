<?php

namespace v2\models;

use common\models\Transaction;
use yii\base\Model;

/**
 * Class TransactionForm.
 * @package v2\models
 * Transaction form model.
 *
 * @property int $period
 * @property int $type
 * @property int $user_id
 */
class TransactionForm extends Model
{

    public $period;
    public $user_id;
    public $type;


    public function rules()
    {
        return [
            [['user_id', 'period', 'type'], 'required'],
            [['period', 'user_id'], 'integer'],
            [['type'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'period' => 'Период',
            'user_id' => 'Пользователь',
            'type' => 'Способ оплаты'
        ];
    }

    public function getData()
    {
        $serverName = \Yii::$app->request->serverName;

        return [
            'receiver' => \Yii::$app->params['yandex_id'],
            'formcomment' => 'Sms555.ru',
            'short-dest' => 'Sms555.ru',
            'quickpay-form' => 'shop',
            'targets' => $this->getTarget(),
            'sum' => $this->getPrice(),
            'label' => $this->getLabel(),
            'paymentType' => $this->type,
            'successURL' => "http://{$serverName}/cabinet/history",
        ];
    }

    public function getTarget()
    {
        $uid = \Yii::$app->user->id;
        return "Информационные услуги. Оплата на {$this->period} дней для пользователя #id{$uid}";
    }

    public function getLabel()
    {
        $uid = \Yii::$app->user->id;
        return "SMS555.{$this->period}.{$uid}";
    }

    public function getPrice()
    {
        return $this->period == 30 ? Transaction::PRICE_30 : Transaction::PRICE_90;
    }


}