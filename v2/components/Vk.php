<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 29.08.2015
 * Time: 13:30
 */

namespace v2\components;

use yii\base\Component;
use yii\helpers\Url;
use yii\helpers\VarDumper;

class Vk extends Component
{

    public function auth($code)
    {
        $data = \Yii::$app->curl->get('https://oauth.vk.com/access_token', [
            'client_id' => \Yii::$app->params['vk_client_id'],
            'client_secret' => \Yii::$app->params['vk_secret'],
            'code' => $code,
            'redirect_uri' => 'http://' . \Yii::$app->request->serverName . Url::to('/main/login/auth'),
        ])->execJson();

        $data->email = isset($data->email) ? $data->email : null;

        return $data;
    }

    public function getProfile($user)
    {
        $data = \Yii::$app->curl->get('https://api.vk.com/method/users.get', [
            'uids' => $user->user_id,
            'fields' => 'uid,first_name,last_name,sex,photo,city,country',
        ])->execJson();

        return $data->response[0];
    }

}