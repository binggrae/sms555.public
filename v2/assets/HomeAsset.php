<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace v2\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HomeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/home.css?2',
        'http://fonts.googleapis.com/css?family=PT+Sans+Narrow:regular&subset=latin,cyrillic'
    ];
    public $js = [
        'js/goto.js',
        'js/menu.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',
        'v2\assets\ModalAsset'
    ];
}
