<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'language'=>'ru-RU',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'v2\controllers',
    'modules' => [
        'admin' => [
            'class' => 'v2\modules\admin\Admin',
        ],
		'backuprestore' => [ 
			'class' => '\oe\modules\backuprestore\Module',
			//'layout' => '@admin-views/layouts/main'
		],
		'gridview' => [
			'class' => '\kartik\grid\Module',
		],
        'main' => [
            'class' => 'v2\modules\main\Main',
        ],
        'cabinet' => [
            'class' => 'v2\modules\cabinet\Cabinet',
        ],
        'transaction' => [
            'class' => 'v2\modules\transaction\Module',
        ],
    ],
    'components' => [
	
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => '/main/default/index',
                '/news' => '/main/default/news',

                '/cabinet' => '/cabinet/default/index',
                '/cabinet/<action:(settings)>' => '/cabinet/default/<action>',

                '/transaction/<action:(run|yandex)>' => '/cabinet/default/<action>',

                '/login' => '/main/login/index',
                '/logout' => '/main/login/logout',
                '/auth' => '/main/login/auth',

                //'<module:\w+>/<controller:\w+>' => '/<module>/<controller>/index',
                'bitrix/<file:.+>' => 'admin/account/static'
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['trace', 'error', 'warning'],
                ],
            ],
        ],

        'errorHandler' => [
            'errorAction' => '/main/site/error',
        ],

        'vk' => [
            'class' => 'v2\components\Vk',
            'client_id' => '4067802',
            'client_secret' => 'RsSxuIXojWZ7D1BHyE6R',
        ],

        'curl' => [
            'class' => 'v2\components\Curl',
        ],
        'curlRequest' => [
            'class' => 'v2\components\Curl',
            'ch' => '',
        ],
    ],
    'params' => $params,
];
