(function($) {
    "use strict";
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 300) {
                $('.go-top').fadeIn();
            } else {
                $('.go-top').fadeOut();
            }
        });
        $('.go-top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 400);
            return false;
        });
    });
})(jQuery);