(function ($) {
    'use strict';
    $(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();

        var hash = window.location.hash
        if (hash) {
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 400);
            window.location.hash = '';
        }

        $('.menu a').click(function() {
			console.log($(this ).data('target'));
			if($(this ).data('target') !== 'stat') {
				$('html, body').animate({
					scrollTop: $($(this).attr('href').substr(1)).offset().top
				}, 400);
				return false;
			}
        });
    })
})(jQuery);